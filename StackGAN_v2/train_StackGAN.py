import torch
from torch.utils.tensorboard import SummaryWriter
from torch import nn
from torch.nn import functional as F
from torchvision import transforms
import torchvision.utils as vutils
from torch import optim
import numpy as np
import random
import pprint
import os
from tqdm import tqdm
import json
import pdb

from args_StackGAN import args
from datasets_StackGAN import FoodDataset
from networks_StackGAN import G_NET, D_NET64, D_NET128, D_NET256
from utils_StackGAN import load_retrieval_model, prepare_data, compute_img_feat, compute_txt_feat, save_img_results

from metrics.frechet_inception_distance import FIDModel, compute_fid
from metrics.inception_score import INCEPTION_V3, compute_is
from metrics.msssim import compute_msssim
from metrics.ndcg import compute_nDCG_by_sim

import sys
sys.path.append('../retrieval_model')
from networks_retrieval import TextEncoder, ImageEncoder, SharedLayer, norm
from utils_retrieval import rank
sys.path.append('../')
from utils import param_counter

##############################
# setup
##############################
if not args.seed:
    args.seed = random.randint(1, 10000)
random.seed(args.seed)
torch.manual_seed(args.seed)
np.random.seed(args.seed)

device = torch.device('cuda' \
    if torch.cuda.is_available() and args.cuda
    else 'cpu')
if device.__str__() == 'cpu':
    args.batch_size = 16

if not args.ca:
    args.kl = 0.0

pp = pprint.PrettyPrinter(indent=2)
pp.pprint(args.__dict__)

##############################
# dataset
##############################
imsize = args.base_size * (2 ** (args.levels-1))
image_transform = transforms.Compose([
    transforms.Resize(int(imsize * 76 / 64)),
    transforms.RandomCrop(imsize),
    transforms.RandomHorizontalFlip()])
train_set = FoodDataset(
    recipe_file=args.recipe_file,
    img_dir=args.img_dir,
    levels=args.levels,
    part='train', 
    food_type=args.food_type,
    base_size=args.base_size, 
    transform=image_transform)

val_set = FoodDataset(
    recipe_file=args.recipe_file,
    img_dir=args.img_dir,
    levels=args.levels,
    part='val', 
    food_type=args.food_type,
    base_size=args.base_size, 
    transform=image_transform)

if args.debug:
    train_set = torch.utils.data.Subset(train_set, range(1000))
    val_set = torch.utils.data.Subset(val_set, range(1000))

train_loader = torch.utils.data.DataLoader(
        train_set, batch_size=args.batch_size,
        drop_last=True, shuffle=True, num_workers=int(args.workers))
val_loader = torch.utils.data.DataLoader(
        val_set, batch_size=64,
        drop_last=True, shuffle=False, num_workers=int(args.workers))
print('train data info:', len(train_set), len(train_loader))
print('val data info:', len(val_set), len(val_loader))

##############################
# model
##############################
text_encoder, image_encoder, shared_layer = load_retrieval_model(args.retrieval_model_ckpt, device)

def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.orthogonal_(m.weight.data, 1.0)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)
    elif classname.find('Linear') != -1:
        nn.init.orthogonal_(m.weight.data, 1.0)
        if m.bias is not None:
            m.bias.data.fill_(0.0)

netG = G_NET(levels=args.levels, ca=args.ca)
netG.apply(weights_init)
netG = torch.nn.DataParallel(netG)
print('# params in netG =', param_counter(netG.parameters()))

netsD = []
netsD.append(D_NET64())
if args.levels >= 2:
    netsD.append(D_NET128())
if args.levels >= 3:
    netsD.append(D_NET256())
for i in range(len(netsD)):
    netsD[i].apply(weights_init)
    netsD[i] = torch.nn.DataParallel(netsD[i])
    print('# params in netD_{} ='.format(i), param_counter(netsD[i].parameters()))

# pdb.set_trace()
##############################
# train
##############################
def KL_loss(mu, logvar):
    # -0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD_element = mu.pow(2).add_(logvar.exp()).mul_(-1).add_(1).add_(logvar)
    KLD = torch.mean(KLD_element).mul_(-0.5)
    return KLD

inception_model = INCEPTION_V3()
fid_model = FIDModel()

netG = netG.to(device)
for i in range(len(netsD)):
    netsD[i] = netsD[i].to(device)
inception_model = inception_model.to(device)
inception_model.eval()
fid_model = fid_model.to(device)
fid_model.eval()

optimizerG = optim.Adam(netG.parameters(),
                        lr=args.lr_g,
                        betas=(0.5, 0.999))
optimizersD = []
num_Ds = len(netsD)
for i in range(num_Ds):
    opt = optim.Adam(netsD[i].parameters(),
                        lr=args.lr_d,
                        betas=(0.5, 0.999))
    optimizersD.append(opt)

criterion = nn.BCELoss()

noise = torch.FloatTensor(args.batch_size, args.z_dim).to(device)
fixed_noise_part1 = torch.FloatTensor(1, args.z_dim).normal_(0, 1)
fixed_noise_part1 = fixed_noise_part1.repeat(32, 1)
fixed_noise_part2 = torch.FloatTensor(32, args.z_dim).normal_(0, 1)
fixed_noise = torch.cat([fixed_noise_part1, fixed_noise_part2], dim=0).to(device)

e_start = 0
e_end = args.epochs
niter = 0
def save_model(epoch, save_dir):
    ckpt = {}
    ckpt['epoch'] = epoch
    ckpt['niter'] = niter - 1
    ckpt['netG'] = netG.state_dict()
    ckpt['optimizerG'] = optimizerG.state_dict()
    for i in range(len(netsD)):
        netD = netsD[i]
        optimizerD = optimizersD[i]
        ckpt['netD_{}'.format(i)] = netD.state_dict()
        ckpt['optimizerD_{}'.format(i)] = optimizerD.state_dict()
    filepath = os.path.join(save_dir, 'e{}.ckpt'.format(epoch))
    print('ckpt path:', filepath)
    torch.save(ckpt, filepath)

def load_model(ckpt_path):
    global e_start, e_end, niter
    print('=> load from checkpoint:', ckpt_path)
    ckpt = torch.load(ckpt_path)
    netG.load_state_dict(ckpt['netG'])
    optimizerG.load_state_dict(ckpt['optimizerG'])
    for i in range(len(netsD)):
        netsD[i].load_state_dict(ckpt['netD_{}'.format(i)])
        optimizersD[i].load_state_dict(ckpt['optimizerD_{}'.format(i)])
    e_start = ckpt['epoch'] + 1
    e_end = e_start + args.epochs
    niter = ckpt['niter'] + 1

if args.resume:
    load_model(args.resume)

def make_saveDir(title, args):
    save_dir = title
    save_dir += '_labels={}'.format(args.labels)
    save_dir += '_inputNoise={}'.format(args.input_noise)
    save_dir += '_uncond={}'.format(args.uncond)
    save_dir += '_cycleTxt={}'.format(args.cycle_txt)
    save_dir += '_cycleImg={}'.format(args.cycle_img)
    save_dir += '_kl={}'.format(args.kl)
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    if args:
        with open(os.path.join(save_dir, 'args.json'), 'w') as f:
            json.dump(args.__dict__, f, indent=2)
    return save_dir

food_type = args.food_type if args.food_type else 'all'
save_dir = make_saveDir('runs_levels={}/{}_samples{}'.format(args.levels, food_type, len(train_set)), args)
writer = SummaryWriter(log_dir=save_dir)

def _val(epoch):
    print('eval')
    real_txt_feats = []
    real_img_feats = []
    fake_img_feats = []
    is_outputs = []
    real_fid_outputs = []
    fake_fid_outputs = []
    text_encoder.eval()
    image_encoder.eval()
    shared_layer.eval()
    netG.eval()
    batch=0
    real_imgs_list = []
    fake_imgs_list = []
    finish_ssim = False
    for data in tqdm(val_loader):
        real_imgs, _, txt = prepare_data(data, device)
        with torch.no_grad():
            txt_feat = compute_txt_feat(txt, text_encoder, shared_layer)
            fake_imgs, mu, logvar = netG(fixed_noise, txt_feat)
            
            real_imgs_list.extend(real_imgs[-1].tolist())
            fake_imgs_list.extend(fake_imgs[-1].tolist())
            if len(fake_imgs_list) > 1000 and not finish_ssim: 
                print('computing ssim')
                ids_sub = np.random.choice(len(fake_imgs_list), 200, replace=False)

                fake_imgs_list = torch.tensor(fake_imgs_list)[ids_sub]
                msssim = compute_msssim(fake_imgs_list[:100], fake_imgs_list[100:])
                writer.add_scalar('msssim', msssim, epoch)
                fake_imgs_list = []

                real_imgs_list = torch.tensor(real_imgs_list)[ids_sub]
                msssim = compute_msssim(real_imgs_list[:100], real_imgs_list[100:])
                writer.add_scalar('real_msssim', msssim, epoch)
                real_imgs_list = []
                
                finish_ssim = True
                print('computing ssim done')
        
            real_txt_feats.append(txt_feat.detach().cpu())

            fake_img = fake_imgs[-1]
            fake_img_feat = compute_img_feat(fake_img, image_encoder, shared_layer)
            fake_img_feats.append(fake_img_feat.detach().cpu())

            real_img = real_imgs[-1]
            real_img_feat = compute_img_feat(real_img, image_encoder, shared_layer)
            real_img_feats.append(real_img_feat.detach().cpu())

            is_output = inception_model(fake_img.detach())
            is_outputs.append(is_output.cpu())

            real_fid_output = fid_model(real_img.detach())
            real_fid_outputs.append(real_fid_output.cpu())
            fake_fid_output = fid_model(fake_img.detach())
            fake_fid_outputs.append(fake_fid_output.cpu())

        if batch == 0:
            writer.add_histogram('mu', mu, epoch)
            writer.add_histogram('std', (0.5*logvar).exp(), epoch)
            real_fake = save_img_results(real_imgs, fake_imgs, save_dir, epoch)
            writer.add_image('real_fake', real_fake, epoch)
        batch += 1

    real_txt_feats = torch.cat(real_txt_feats, dim=0)
    real_img_feats = torch.cat(real_img_feats, dim=0)
    fake_img_feats = torch.cat(fake_img_feats, dim=0)
    retrieved_range = min(1000, len(val_loader)*args.batch_size)
    print('retrieved_range = {:d}'.format(retrieved_range))
    medR, medR_std, recalls = rank(real_txt_feats.numpy(), real_img_feats.numpy(), retrieved_type='recipe', retrieved_range=retrieved_range)
    writer.add_scalar('real MedR', medR, epoch)
    print('=> [MedR] real: {:.4f}({:.4f})'.format(medR, medR_std))
    medR, medR_std, recalls = rank(real_txt_feats.numpy(), fake_img_feats.numpy(), retrieved_type='recipe', retrieved_range=retrieved_range)
    writer.add_scalar('fake MedR', medR, epoch)
    print('=> [MedR] fake: {:.4f}({:.4f})'.format(medR, medR_std))

    is_outputs = torch.cat(is_outputs, 0)
    mean_is, std_is = compute_is(is_outputs, 2)
    writer.add_scalar('IS', mean_is, epoch)

    real_fid_outputs = torch.cat(real_fid_outputs, 0)
    fake_fid_outputs = torch.cat(fake_fid_outputs, 0)
    fid = compute_fid(real_fid_outputs, fake_fid_outputs)
    writer.add_scalar('FID', fid, epoch)

    nDCGs, worst_nDCGs = compute_nDCG_by_sim(real_img_feats.numpy(), real_txt_feats.numpy(), retrieved_range=retrieved_range, save_dir=save_dir, where='real')
    print('=> nDCG (real): {:.4f}({:.4f})'.format(nDCGs.mean(), nDCGs.std()))
    print('=> worst possible nDCG (real): {:.4f}({:.4f})'.format(worst_nDCGs.mean(), worst_nDCGs.std()))
    writer.add_scalar('real nDCG', nDCGs.mean(), epoch)
    nDCGs, worst_nDCGs = compute_nDCG_by_sim(fake_img_feats.numpy(), real_txt_feats.numpy(), retrieved_range=retrieved_range, save_dir=save_dir, where='fake')
    print('=> nDCG (fake): {:.4f}({:.4f})'.format(nDCGs.mean(), nDCGs.std()))
    print('=> worst possible nDCG (fake): {:.4f}({:.4f})'.format(worst_nDCGs.mean(), worst_nDCGs.std()))
    writer.add_scalar('fake nDCG', nDCGs.mean(), epoch)

    print('saving model after epoch {}'.format(epoch))
    save_model(epoch, save_dir)


def compute_cycle_loss(feat1, feat2, paired=True):
    if paired:
        loss = nn.CosineEmbeddingLoss(0.3)(feat1, feat2, torch.ones(feat1.shape[0]).to(device))
    else:
        loss = nn.CosineEmbeddingLoss(0.3)(feat1, feat2, -torch.ones(feat1.shape[0]).to(device))
    return loss

def compute_kl(mu, logvar):
    # -0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp(), dim=1)
    return KLD.mean() / args.embedding_dim # not correct, this is just to follow the official code

def _train(epoch):
    global niter
    print('train')
    epoch_loss_G = 0.0
    epoch_loss_D = 0.0
    epoch_loss_kl = 0.0
    netG.train()
    for data in tqdm(train_loader):
        if args.labels == 'original':
            real_labels = torch.FloatTensor(args.batch_size).fill_(
                1)  # (torch.FloatTensor(args.batch_size).uniform_() < 0.9).float() #
            fake_labels = torch.FloatTensor(args.batch_size).fill_(
                0)  # (torch.FloatTensor(args.batch_size).uniform_() > 0.9).float() #
        elif args.labels == 'R-smooth':
            real_labels = torch.FloatTensor(args.batch_size).fill_(1) - (
                        torch.FloatTensor(args.batch_size).uniform_() * 0.1)
            fake_labels = (torch.FloatTensor(args.batch_size).uniform_() * 0.1)
        elif args.labels == 'R-flip':
            real_labels = (torch.FloatTensor(args.batch_size).uniform_() < 0.9).float()  #
            fake_labels = (torch.FloatTensor(args.batch_size).uniform_() > 0.9).float()  #
        elif args.labels == 'R-flip-smooth':
            real_labels = torch.abs((torch.FloatTensor(args.batch_size).uniform_() > 0.9).float() - (
                    torch.FloatTensor(args.batch_size).fill_(1) - (
                        torch.FloatTensor(args.batch_size).uniform_() * 0.1)))
            fake_labels = torch.abs((torch.FloatTensor(args.batch_size).uniform_() > 0.9).float() - (
                    torch.FloatTensor(args.batch_size).uniform_() * 0.1))

        real_labels = real_labels.to(device)
        fake_labels = fake_labels.to(device)
        
        real_imgs, wrong_imgs, txt = prepare_data(data, device)
        with torch.no_grad():
            txt_feat = compute_txt_feat(txt, text_encoder, shared_layer)
        
        # (1) generate fake images
        noise.normal_(0, 1)
        fake_imgs, mu, logvar = netG(noise, txt_feat)

        # (2) update D networks
        errD_total = 0
        for level in range(args.levels):
            if args.input_noise:
                sigma = np.clip(1.0 - epoch/200, 0, 1) * 0.1
                real_img_noise = torch.empty_like(real_imgs[level]).normal_(0, sigma)
                wrong_img_noise = torch.empty_like(wrong_imgs[level]).normal_(0, sigma)
                fake_img_noise = torch.empty_like(fake_imgs[level]).normal_(0, sigma)
            else:
                real_img_noise = torch.zeros_like(real_imgs[level])
                wrong_img_noise = torch.zeros_like(wrong_imgs[level])
                fake_img_noise = torch.zeros_like(fake_imgs[level])

            #forward
            netD = netsD[level]
            optD = optimizersD[level]
            
            real_logits = netD(real_imgs[level]+real_img_noise, mu.detach())
            wrong_logits = netD(wrong_imgs[level]+wrong_img_noise, mu.detach())
            fake_logits = netD(fake_imgs[level].detach()+fake_img_noise, mu.detach())

            errD_real = criterion(real_logits[0], real_labels) # cond_real --> 1
            errD_wrong = criterion(wrong_logits[0], fake_labels) # cond_wrong --> 0
            errD_fake = criterion(fake_logits[0], fake_labels) # cond_fake --> 0
            
            if len(real_logits)>1:
                errD_cond = errD_real + errD_wrong + errD_fake
                errD_real_uncond = criterion(real_logits[1], real_labels) # uncond_real --> 1
                errD_wrong_uncond = criterion(wrong_logits[1], real_labels) # uncond_wrong --> 1
                errD_fake_uncond = criterion(fake_logits[1], fake_labels) # uncond_fake --> 0
                errD_uncond = errD_real_uncond + errD_wrong_uncond + errD_fake_uncond
            else: # back to GAN-INT-CLS
                errD_cond = errD_real + 0.5 * (errD_wrong + errD_fake)
                errD_uncond = 0.0
            
            errD = errD_cond + args.uncond * errD_uncond
            
            optD.zero_grad()
            errD.backward()
            optD.step()

            # record
            errD_total += errD
            # if level == 2:
            writer.add_scalar('D_loss_cond{}'.format(level), errD_cond, niter)
            writer.add_scalar('D_loss_uncond{}'.format(level), errD_uncond, niter)
            writer.add_scalar('D_loss{}'.format(level), errD, niter)
        
        # (3) Update G network: maximize log(D(G(z)))
        # forward
        errG_total = 0.0
        # txt_feat = compute_txt_feat(txt, text_encoder, shared_layer)
        # fake_imgs, mu, logvar = netG(noise, txt_feat)
        for level in range(args.levels):
            if args.input_noise:
                sigma = np.clip(1.0 - epoch/200, 0, 1) * 0.1
                fake_img_noise = torch.empty_like(fake_imgs[level]).normal_(0, sigma)
            else:
                fake_img_noise = torch.zeros_like(fake_imgs[level])

            outputs = netsD[level](fake_imgs[level] + fake_img_noise, mu)
            errG_cond = criterion(outputs[0], real_labels) # cond_fake --> 1
            errG_uncond = criterion(outputs[1], real_labels) # uncond_fake --> 1

            fake_img_feat = compute_img_feat(fake_imgs[level], image_encoder, shared_layer)
            errG_cycle_txt = compute_cycle_loss(fake_img_feat, txt_feat)
            
            real_img_feat = compute_img_feat(real_imgs[level], image_encoder, shared_layer)
            errG_cycle_img = compute_cycle_loss(fake_img_feat, real_img_feat)

            # rightRcp_vs_rightImg = compute_cycle_loss(txt_feat, real_img_feat)
            # wrong_img_feat = compute_img_feat(wrong_imgs[level], image_encoder, shared_layer)
            # rightRcp_vs_wrongImg = compute_cycle_loss(txt_feat, wrong_img_feat, paired=False)
            # tri_loss = rightRcp_vs_rightImg + rightRcp_vs_wrongImg
            
            errG = errG_cond \
                + args.uncond * errG_uncond \
                    + args.cycle_txt * errG_cycle_txt \
                        + args.cycle_img * errG_cycle_img \
                            # + args.tri_loss * tri_loss

            # record
            errG_total += errG
            # if level == 2:
            # writer.add_scalar('G_tri_loss{}'.format(level), tri_loss, niter)
            writer.add_scalar('G_loss_cond{}'.format(level), errG_cond, niter)
            writer.add_scalar('G_loss_uncond{}'.format(level), errG_uncond, niter)
            writer.add_scalar('G_loss_cycle_txt{}'.format(level), errG_cycle_txt, niter)
            writer.add_scalar('G_loss_cycle_img{}'.format(level), errG_cycle_img, niter)
            writer.add_scalar('G_loss{}'.format(level), errG, niter)
        
        errG_kl = compute_kl(mu, logvar)
        writer.add_scalar('G_kl', errG_kl, niter)
        errG_total += args.kl * errG_kl

        optimizerG.zero_grad()
        errG_total.backward()
        optimizerG.step()
            
        # record
        writer.add_scalar('D_loss', errD_total, niter)
        writer.add_scalar('G_loss', errG_total, niter)
        epoch_loss_D += errD_total
        epoch_loss_G += errG_total
        epoch_loss_kl += errG_kl

        niter += 1
    
    # end of the epoch
    epoch_loss_D /= len(train_loader)
    epoch_loss_G /= len(train_loader)
    epoch_loss_kl /= len(train_loader)
    print('Loss_D={:.4f}, Loss_G={:.4f}, Loss_kl={:.4f}'.format(
        epoch_loss_D, epoch_loss_G, epoch_loss_kl
    ))
    # record
    writer.add_scalar('epoch_loss_D', epoch_loss_D, epoch)
    writer.add_scalar('epoch_loss_G', epoch_loss_G, epoch)
    writer.add_scalar('epoch_loss_kl', epoch_loss_kl, epoch)


for epoch in range(e_start, e_end):
    print('-'*40)
    print('Epoch {}/{}'.format(epoch, e_end-1))
    _val(epoch)
    _train(epoch)

