from datasets_StackGAN import TextDataset
import sys
sys.path.append('../retrieval_model')
from networks_retrieval import TextEncoder, ImageEncoder, SharedLayer, norm
import torch
from torch import nn
from torchvision import transforms
from tqdm import tqdm

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--part', type=str, default='train')
args = parser.parse_args()

device = torch.device('cuda')
dataset = TextDataset(
    recipe_file='../data/Recipe1M/recipes_withImage.json',
    split=args.part)
print(len(dataset))
dataloader = torch.utils.data.DataLoader(dataset, batch_size=4096, shuffle=False,
    num_workers=8, pin_memory=True, drop_last=False)

text_encoder = TextEncoder(
    emb_dim=300, 
    hid_dim=300, 
    z_dim=1024, 
    word2vec_file='../retrieval_model/models/word2vec_recipes.bin', 
    with_attention=True, 
    with_word2vec=True)

shared_layer = SharedLayer(z_dim=1024)

text_encoder, shared_layer = [nn.DataParallel(x).to(device).eval() for x in [text_encoder, shared_layer]]

ckpt = torch.load('../retrieval_model/models/recipe_hardMining_sharedLayer.ckpt')
text_encoder.load_state_dict(ckpt['text_encoder'])
shared_layer.load_state_dict(ckpt['shared_layer'])

real_txt_feats = []

for data in tqdm(dataloader):
    txt = [x.to(device) for x in data]
    with torch.no_grad():
        txt_modal, alpha_title, alpha_ingredients, alpha_instructions, alpha_words = text_encoder(*txt)
        rec_id_fea = norm(shared_layer(txt_modal))
    real_txt_feats.append(rec_id_fea.detach().cpu())

real_txt_feats = torch.cat(real_txt_feats, dim=0)
torch.save(real_txt_feats, '{}_txt_feats.pt'.format(args.part))