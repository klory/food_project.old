from types import SimpleNamespace
import json
import sys
import torch
from torch import nn
from torch.nn import functional as F
import torchvision.utils as vutils
import os
sys.path.append('../retrieval_model')
from networks_retrieval import TextEncoder, ImageEncoder, SharedLayer, norm

def prepare_data(data, device):
    imgs, w_imgs, txt, _ = data
    real_vimgs, wrong_vimgs = [], []
    for i in range(len(imgs)):
        real_vimgs.append(imgs[i].to(device))
        wrong_vimgs.append(w_imgs[i].to(device))
    vtxt = [x.to(device) for x in txt]
    return real_vimgs, wrong_vimgs, vtxt

def _find_args(filepath):
    prefix = filepath.rsplit('.', 1)[0]
    args_file = prefix + '.json'
    # print('encoder args file:', args_file)
    assert os.path.exists(args_file), 'args.json is not found'
    with open(args_file, 'r') as f:
        args = json.load(f)
    args = SimpleNamespace(**args)
    return args

def load_retrieval_model(filepath, device):
    args_retrieval_model = _find_args(filepath)
    text_encoder = TextEncoder(
        emb_dim=args_retrieval_model.word2vec_dim, 
        hid_dim=args_retrieval_model.rnn_hid_dim, 
        z_dim=args_retrieval_model.feature_dim, 
        word2vec_file='../retrieval_model/models/word2vec_recipes.bin', 
        with_attention=args_retrieval_model.with_attention, 
        with_word2vec=args_retrieval_model.with_word2vec)
    image_encoder = ImageEncoder(
        z_dim=args_retrieval_model.feature_dim)
    shared_layer = SharedLayer(z_dim=args_retrieval_model.feature_dim)
    text_encoder, image_encoder, shared_layer = [nn.DataParallel(x).eval().to(device) for x in [text_encoder, image_encoder, shared_layer]]
    print('load from:', filepath)
    ckpt = torch.load(filepath)
    text_encoder.load_state_dict(ckpt['text_encoder'])
    image_encoder.load_state_dict(ckpt['image_encoder'])
    shared_layer.load_state_dict(ckpt['shared_layer'])
    return text_encoder, image_encoder, shared_layer


def compute_txt_feat(txt, text_encoder, shared_layer):
    txt_feat, _, _, _, _ = text_encoder(*txt)
    return norm(shared_layer(txt_feat))

def compute_img_feat(img, image_encoder, shared_layer):
    mean = [0.485, 0.456, 0.406]
    std = [0.229, 0.224, 0.225]
    img = img/2 + 0.5
    img = F.interpolate(img, [224, 224], mode='bilinear', align_corners=True)
    for i in range(img.shape[1]):
        img[:,i] = (img[:,i]-mean[i])/std[i]
    feat = norm(shared_layer(image_encoder(img)))
    return feat

def save_img_results(real_imgs, fake_imgs, save_dir, epoch, level=-1):
    num = 64
    real_img = real_imgs[level][0:num]
    fake_img = fake_imgs[level][0:num]
    real_fake = torch.stack([real_img, fake_img]).permute(1,0,2,3,4).contiguous()
    real_fake = real_fake.view(-1, real_fake.shape[-3], real_fake.shape[-2], real_fake.shape[-1])
    vutils.save_image(
            real_fake, 
            '{}/e{}_real_fake.png'.format(save_dir, epoch),  
            normalize=True, scale_each=True)
    real_fake = vutils.make_grid(real_fake, normalize=True, scale_each=True)
    vutils.save_image(
            fake_img, 
            '{}/e{}_fake_samples.png'.format(save_dir, epoch), 
            normalize=True, scale_each=True)
    return real_fake