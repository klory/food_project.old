import torch
from torch import nn
from torchvision import transforms
import torchvision.utils as vutils
import numpy as np
import random
import os
from tqdm import tqdm

from args_StackGAN import args
from datasets_StackGAN import FoodDataset
from networks_StackGAN import G_NET
from utils_StackGAN import load_retrieval_model, prepare_data, compute_img_feat, compute_txt_feat, save_img_results

from metrics.frechet_inception_distance import FIDModel, compute_fid
from metrics.inception_score import INCEPTION_V3, compute_is
from metrics.msssim import compute_msssim

import sys
sys.path.append('../retrieval_model')
from networks_retrieval import TextEncoder, ImageEncoder, SharedLayer, norm
from utils_retrieval import rank
sys.path.append('../')
import utils

from matplotlib import pyplot as plt
import pdb

retrieved_range = 1000

if not args.resume:
    args.resume = 'models/salad_e171.ckpt'

if not args.seed:
    args.seed = random.randint(1, 10000)
random.seed(args.seed)
torch.manual_seed(args.seed)
np.random.seed(args.seed)

device = torch.device('cuda')

imsize = args.base_size * (2 ** (args.levels-1))
image_transform = transforms.Compose([
    transforms.Resize(int(imsize * 76 / 64)),
    transforms.CenterCrop(imsize)
])

dataset = FoodDataset(
    recipe_file=args.recipe_file,
    img_dir=args.img_dir,
    levels=args.levels,
    part='test', 
    food_type=args.food_type,
    base_size=args.base_size, 
    transform=image_transform,
    num_samples=retrieved_range)

recipes = dataset.recipes

dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=64,
        drop_last=False, shuffle=False, num_workers=int(args.workers))
print('test data info:', len(dataset), len(dataloader))

##############################
# model
##############################
text_encoder, image_encoder, shared_layer = load_retrieval_model(args.retrieval_model_ckpt, device)

netG = G_NET()
netG = nn.DataParallel(netG).to(device).eval()
ckpt = torch.load(args.resume)
netG.load_state_dict(ckpt['netG'])
epoch = ckpt['epoch']

##############################
# test
##############################
model_name = os.path.basename(args.resume).rsplit('.')[0]
res = 64 * 2**(args.level)
save_dir = 'models/{}/{}/'.format(model_name, 'analyze_rank')
if not os.path.exists(save_dir):
    os.makedirs(save_dir)
one_noise = torch.FloatTensor(1, args.z_dim).normal_(0, 1).to(device)

real_txt_feats = []
real_img_feats = []
fake_img_feats = []

text_encoder.eval()
image_encoder.eval()
shared_layer.eval()
netG.eval()

real_imgs_list = []
fake_imgs_list = []
print('get text and image features...')
for data in tqdm(dataloader):
    real_imgs, _, txt = prepare_data(data, device)
    with torch.no_grad():
        txt_feat = compute_txt_feat(txt, text_encoder, shared_layer)
        fixed_noise = one_noise.repeat(txt_feat.shape[0], 1)
        fake_imgs, mu, logvar = netG(fixed_noise, txt_feat)
        
        real_imgs_list.extend(real_imgs[-1].tolist())
        fake_imgs_list.extend(fake_imgs[-1].tolist())
    
        real_txt_feats.append(txt_feat.detach().cpu())

        fake_img = fake_imgs[-1]
        fake_img_feat = compute_img_feat(fake_img, image_encoder, shared_layer)
        fake_img_feats.append(fake_img_feat.detach().cpu())

        real_img = real_imgs[-1]
        real_img_feat = compute_img_feat(real_img, image_encoder, shared_layer)
        real_img_feats.append(real_img_feat.detach().cpu())

real_txt_feats = torch.cat(real_txt_feats, dim=0).numpy()
real_img_feats = torch.cat(real_img_feats, dim=0).numpy()
fake_img_feats = torch.cat(fake_img_feats, dim=0).numpy()

from nltk.translate.bleu_score import sentence_bleu, SmoothingFunction
def get_ranks(img_feats, txt_feats):
    N = txt_feats.shape[0]
    idxs = range(N)
    img_feats = img_feats / np.linalg.norm(img_feats, axis=1)[:, None]
    txt_feats = txt_feats / np.linalg.norm(txt_feats, axis=1)[:, None]
    sims = np.dot(img_feats, txt_feats.T) # [N, N]
    ranks = []
    full_ranks = []
    front_bleus = []
    back_bleus = []
    # loop through the N similarities for images
    for ii in tqdm(idxs):
        # get a column of similarities for image ii
        sim = sims[ii,:]
        # sort indices in descending order
        sorting = np.argsort(sim)[::-1].tolist()
        # find where the index of the pair sample ended up in the sorting
        pos = sorting.index(ii)
        # store the position
        ranks.append(pos+1)
        full_ranks.append(sorting)

        # compute BLEU
        if len(front_bleus) < 5:
            candidate = (' '.join(recipes[ii]['instructions'])).split()
            cc = SmoothingFunction()
            scores = []
            for i in sorting[:pos]:
                reference = (' '.join(recipes[i]['instructions'])).split()
                score = sentence_bleu([reference], candidate, smoothing_function=cc.method4)
                scores.append(score)
            if not scores:
                front_bleus.append(1.0)
            else:
                front_bleus.append(np.mean(scores))

            scores = []
            for i in sorting[pos+1:]:
                reference = (' '.join(recipes[i]['instructions'])).split()
                score = sentence_bleu([reference], candidate, smoothing_function=cc.method4)
                scores.append(score)
            if not scores:
                back_bleus.append(0.0)
            else:
                back_bleus.append(np.mean(scores))

    print('front BLEU = {:.2f}, back BLEU = {:.2f}'.format(np.mean(front_bleus), np.mean(back_bleus)))
    return np.array(ranks), np.array(full_ranks)


def draw_hist(ranks, where):
    plt.figure()
    n, bins, patches = plt.hist(x=ranks, bins='auto', color='#0504aa', alpha=0.7, rwidth=0.85)
    plt.grid(axis='y', alpha=0.75)
    plt.xlabel('Rank')
    plt.ylabel('Frequency')
    plt.title('{} Rank Distribution'.format(where))
    plt.text(23, 45, '{}({})'.format(np.mean(ranks), np.std(ranks)))
    plt.savefig('{}/{}_distribution_range{}.png'.format(save_dir, where, retrieved_range))

print('get ranks')
fake_ranks, full_fake_ranks = get_ranks(fake_img_feats, real_txt_feats)
print('fake MedR = {:.2f} ({:.2f})'.format(np.median(fake_ranks), np.std(fake_ranks)))
draw_hist(fake_ranks, 'fake')
real_ranks, full_real_ranks = get_ranks(real_img_feats, real_txt_feats)
print('real MedR = {:.2f} ({:.2f})'.format(np.median(real_ranks), np.std(real_ranks)))
draw_hist(real_ranks, 'real')

real_imgs_list = torch.tensor(real_imgs_list)
fake_imgs_list = torch.tensor(fake_imgs_list)

real_imgs_list = (real_imgs_list - real_imgs_list.min()) / (real_imgs_list.max() - real_imgs_list.min())
fake_imgs_list = (fake_imgs_list - fake_imgs_list.min()) / (fake_imgs_list.max() - fake_imgs_list.min())


from PIL import ImageDraw
from PIL import ImageFont
to_pil = transforms.ToPILImage()
to_tensor = transforms.ToTensor()

# sort by fake_rank, then -real_rank
# load original recipes
print('load original recipes (20 seconds)')
original_recipes = utils.Layer.merge(
    [utils.Layer.L1, utils.Layer.L2, utils.Layer.INGRS], 
    os.path.join('../data/Recipe1M/', 'texts'))
original_recipes = {x['id']: x for x in original_recipes}

print('sort by fake ranks(increasing), then real ranks(decreasing)')
tpl = zip(fake_ranks, real_ranks, list(range(len(recipes))))
tpl_sorted = sorted(tpl, key=lambda x:(x[0], -x[1]))
# pdb.set_trace()
def draw_recipe_text(recipe, prefix=''):
    one_title = recipe['title']
    one_ingredients = recipe['ingredients']
    one_instructions = ['{}. '.format(ii+1)+x for ii,x in enumerate(recipe['instructions'])]
    text = prefix + '\n'
    text += '[Title]\n' + one_title + '\n'
    text += '[Ingredients]\n' + ', '.join(one_ingredients) + '\n'
    text += '[Instructions]\n' + '\n'.join(one_instructions)
    txt_component = plt.text(-0.2, 0.5, text, fontsize=6, style='oblique', ha='left', va='center', wrap=True)
    txt_component._get_wrap_line_width = lambda : 280.
    plt.axis('off')
    # plt.tick_params(
    #     axis='both',          # changes apply to the x-axis
    #     which='both',      # both major and minor ticks are affected
    #     top=False, labeltop=False,
    #     bottom=False, labelbottom=False,
    #     left=False, labelleft=False,
    #     right=False, labelright=False)

print('Now we are in the program of saving samples')
i = 0
while 0 <= i < len(tpl_sorted):
    plt.figure(figsize=(12,6))
    one_sample = tpl_sorted[i]
    src_idx = one_sample[-1] # recipe index with the best fake rank and worst real rank
    one_recipe = recipes[src_idx] # real recipe
    one_fake = fake_imgs_list[src_idx].numpy().transpose(1,2,0) # fake image
    one_real = real_imgs_list[src_idx].numpy().transpose(1,2,0) # paired real image
    one_full_real_ranks = full_real_ranks[src_idx] # contains all real ranks indices sorted from best to worst
    gold_index = one_full_real_ranks[0]
    gold_recipe = recipes[gold_index]
    silver_index = one_full_real_ranks[1]
    silver_recipe = recipes[silver_index]

    plt.subplot(2,3,1)
    plt.imshow(one_fake)
    plt.axis('off')
    plt.title('fake rank = {}'.format(one_sample[0]))

    plt.subplot(2,3,2)
    draw_recipe_text(one_recipe, 'real recipe')

    plt.subplot(2,3,4)
    plt.imshow(one_real)
    plt.axis('off')
    plt.title('real rank = {}'.format(one_sample[1]))

    plt.subplot(2,3,5)
    one_origin = original_recipes[one_recipe['id']]
    one_title = one_origin['title']
    one_ingredients = [x['text'] for x in one_origin['ingredients']]
    one_instructions = ['{}. '.format(ii+1)+x['text'] for ii,x in enumerate(one_origin['instructions'])]
    text = ''
    text += '[Title]\n' + one_title + '\n'
    text += '[Ingredients]\n' + ', '.join(one_ingredients) + '\n'
    text += '[Instructions]\n' + '\n'.join(one_instructions)
    txt_component = plt.text(-0.2, 0.5, text, fontsize=6, ha='left', va='center', wrap=True)
    txt_component._get_wrap_line_width = lambda : 280.
    plt.axis('off')
    # plt.tick_params(
    #     axis='both',          # changes apply to the x-axis
    #     which='both',      # both major and minor ticks are affected
    #     top=False, labeltop=False,
    #     bottom=False, labelbottom=False,
    #     left=False, labelleft=False,
    #     right=False, labelright=False)

    plt.subplot(2,3,3)
    draw_recipe_text(gold_recipe, 'real rank = 1')

    plt.subplot(2,3,6)
    draw_recipe_text(silver_recipe, 'real rank = 2')

    plt.savefig(os.path.join(save_dir, 'sample_{}.jpg'.format(i)))
    i = input('Type in recipe index: ')
    while not i.isdigit():
        i = input('Type in recipe index (positive integer): ')
    i = int(i)
else:
    print('Program ends and go to next task')

# sort by fake ranks
print('sort by fake ranks(increasing)')
ind_sorted_by_fake = np.argsort(fake_ranks)
real_ranks_sorted_by_fake = real_ranks[ind_sorted_by_fake]
fake_ranks_sorted_by_fake = fake_ranks[ind_sorted_by_fake]

plt.figure()
plt.plot(real_ranks_sorted_by_fake, label='real_ranks_sorted_by_fake')
plt.plot(fake_ranks_sorted_by_fake, label='fake_ranks_sorted_by_fake')
plt.xlabel('recipe index')
plt.ylabel('rank')
plt.title('Ranks Sorted by Fake')
plt.legend()
plt.savefig(os.path.join(save_dir, 'ranks_sorted_by_fake'))

real_imgs_list_sorted = real_imgs_list[ind_sorted_by_fake]
fake_imgs_list_sorted = fake_imgs_list[ind_sorted_by_fake]
batch = 0
while batch*64 < fake_imgs_list_sorted.shape[0]:
    real_ranks_sub = real_ranks_sorted_by_fake[batch*64: (batch+1)*64]
    fake_ranks_sub = fake_ranks_sorted_by_fake[batch*64: (batch+1)*64]
    print('batch = {}, Real MedR={:.2f}, Fake MedR={:.2f}'.format(batch, np.median(real_ranks_sub), np.median(fake_ranks_sub)))
    real_before = real_imgs_list_sorted[batch*64: (batch+1)*64]
    fake_before = fake_imgs_list_sorted[batch*64: (batch+1)*64] # [64, 3, 224, 224]
    real, fake = [], []
    
    for i, (one_real, one_fake) in enumerate(zip(real_before, fake_before)):
        pil_img = to_pil(one_real)
        draw = ImageDraw.Draw(pil_img)
        draw.text((0, 0), '{:d}'.format(int(real_ranks_sub[i])), (255,255,255))
        img = to_tensor(pil_img)
        real.append(img)

        pil_img = to_pil(one_fake)
        draw = ImageDraw.Draw(pil_img)
        draw.text((0, 0), '{:d}'.format(int(fake_ranks_sub[i])), (255,255,255))
        img = to_tensor(pil_img)
        fake.append(img)
    
    real = torch.stack(real, dim=0)
    fake = torch.stack(fake, dim=0)
    real_fake = torch.stack([real, fake]).permute(1,0,2,3,4).contiguous()
    real_fake = real_fake.view(-1, real_fake.shape[-3], real_fake.shape[-2], real_fake.shape[-1])
    vutils.save_image(
        real_fake, 
        os.path.join(save_dir, 'fake_batch{}.jpg'.format(batch)),
        normalize=True, scale_each=True)
    batch += 1

# sort by real ranks
print('sort by real ranks(increasing)')
ind_sorted_by_real = np.argsort(real_ranks)
real_ranks_sorted_by_real = real_ranks[ind_sorted_by_real]
fake_ranks_sorted_by_real = fake_ranks[ind_sorted_by_real]

plt.figure()
plt.plot(real_ranks_sorted_by_real, label='real_ranks_sorted_by_real')
plt.plot(fake_ranks_sorted_by_real, label='fake_ranks_sorted_by_real')
plt.xlabel('recipe index')
plt.ylabel('rank')
plt.title('Ranks Sorted by real')
plt.legend()
plt.savefig(os.path.join(save_dir, 'ranks_sorted_by_real'))

real_imgs_list_sorted = real_imgs_list[ind_sorted_by_real]
fake_imgs_list_sorted = fake_imgs_list[ind_sorted_by_real]
batch = 0
while batch*64 < fake_imgs_list_sorted.shape[0]:
    real_ranks_sub = real_ranks_sorted_by_real[batch*64: (batch+1)*64]
    fake_ranks_sub = fake_ranks_sorted_by_real[batch*64: (batch+1)*64]
    print('batch = {}, Real MedR={:.2f}, Fake MedR={:.2f}'.format(batch, np.median(real_ranks_sub), np.median(fake_ranks_sub)))
    real_before = real_imgs_list_sorted[batch*64: (batch+1)*64]
    fake_before = fake_imgs_list_sorted[batch*64: (batch+1)*64] # [64, 3, 224, 224]
    real, fake = [], []
    
    for i, (one_real, one_fake) in enumerate(zip(real_before, fake_before)):
        pil_img = to_pil(one_real)
        draw = ImageDraw.Draw(pil_img)
        draw.text((0, 0), '{:d}'.format(int(real_ranks_sub[i])), (0,0,255))
        img = to_tensor(pil_img)
        real.append(img)

        pil_img = to_pil(one_fake)
        draw = ImageDraw.Draw(pil_img)
        draw.text((0, 0), '{:d}'.format(int(fake_ranks_sub[i])), (0,0,255))
        img = to_tensor(pil_img)
        fake.append(img)
    
    real = torch.stack(real, dim=0)
    fake = torch.stack(fake, dim=0)
    real_fake = torch.stack([real, fake]).permute(1,0,2,3,4).contiguous()
    real_fake = real_fake.view(-1, real_fake.shape[-3], real_fake.shape[-2], real_fake.shape[-1])
    vutils.save_image(
        real_fake, 
        os.path.join(save_dir, 'real_batch{}.jpg'.format(batch)), 
        normalize=True, scale_each=True)
    batch += 1

