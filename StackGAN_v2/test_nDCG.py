import torch
from torch import nn
from torchvision import transforms
import torchvision.utils as vutils
import numpy as np
import random
import os
from tqdm import tqdm

from args_StackGAN import args
from datasets_StackGAN import FoodDataset
from networks_StackGAN import G_NET
from utils_StackGAN import load_retrieval_model, prepare_data, compute_img_feat, compute_txt_feat, save_img_results

from metrics.ndcg import compute_nDCG_by_sim, compute_nDCG_by_bleu

import sys
sys.path.append('../retrieval_model')
from networks_retrieval import TextEncoder, ImageEncoder, SharedLayer, norm
from utils_retrieval import rank

if not args.resume:
    args.resume = 'models/salad_e171.ckpt'

if not args.seed:
    args.seed = random.randint(1, 10000)
random.seed(args.seed)
torch.manual_seed(args.seed)
np.random.seed(args.seed)
print(args.seed)

device = torch.device('cuda')

imsize = args.base_size * (2 ** (args.levels-1))
image_transform = transforms.Compose([
    transforms.Resize(int(imsize * 76 / 64)),
    transforms.CenterCrop(imsize)
])

dataset = FoodDataset(
    recipe_file=args.recipe_file,
    img_dir=args.img_dir,
    levels=args.levels,
    part='test', 
    food_type=args.food_type,
    base_size=args.base_size, 
    transform=image_transform)

recipes = dataset.recipes

if args.debug:
    print('in debug mode')
    dataset = torch.utils.data.Subset(dataset, range(200))

dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=args.batch_size,
        drop_last=True, shuffle=False, num_workers=int(args.workers))
print('test data info:', len(dataset), len(dataloader))

recipes = recipes[:args.batch_size*len(dataloader)]

##############################
# model
##############################
text_encoder, image_encoder, shared_layer = load_retrieval_model(args.retrieval_model_ckpt, device)
netG = nn.DataParallel(G_NET()).to(device)
ckpt = torch.load(args.resume)
netG.load_state_dict(ckpt['netG'])

##############################
# test
##############################
model_name = os.path.basename(args.resume).rsplit('.')[0]
folder_name = 'test_nDCG'
save_dir = 'models/{}/{}/'.format(model_name, folder_name)
if not os.path.exists(save_dir):
    os.makedirs(save_dir)
fixed_noise = torch.FloatTensor(1, args.z_dim).normal_(0, 1).to(device)
fixed_noise = fixed_noise.repeat(args.batch_size, 1)

real_txt_feats = []
real_img_feats = []
fake_img_feats = []

text_encoder.eval()
image_encoder.eval()
shared_layer.eval()
netG.eval()

for data in tqdm(dataloader):
    real_imgs, _, txt = prepare_data(data, device)
    with torch.no_grad():
        txt_feat = compute_txt_feat(txt, text_encoder, shared_layer)
        fake_imgs, mu, logvar = netG(fixed_noise, txt_feat)
        
        real_txt_feats.append(txt_feat.detach().cpu())

        real_img = real_imgs[-1]
        real_img_feat = compute_img_feat(real_img, image_encoder, shared_layer)
        real_img_feats.append(real_img_feat.detach().cpu())

        fake_img = fake_imgs[-1]
        fake_img_feat = compute_img_feat(fake_img, image_encoder, shared_layer)
        fake_img_feats.append(fake_img_feat.detach().cpu())

real_txt_feats = torch.cat(real_txt_feats, dim=0)
real_img_feats = torch.cat(real_img_feats, dim=0)
fake_img_feats = torch.cat(fake_img_feats, dim=0)
retrieved_range = min(900, len(dataloader)*args.batch_size)

nDCGs, worst_nDCGs = compute_nDCG_by_sim(real_img_feats.numpy(), real_txt_feats.numpy(), retrieved_range=retrieved_range, save_dir=save_dir, where='real')
print('=> nDCG (real): {:.4f}({:.4f})'.format(nDCGs.mean(), nDCGs.std()))
print('=> worst possible nDCG (real): {:.4f}({:.4f})'.format(worst_nDCGs.mean(), worst_nDCGs.std()))
nDCGs, worst_nDCGs = compute_nDCG_by_sim(fake_img_feats.numpy(), real_txt_feats.numpy(), retrieved_range=retrieved_range, save_dir=save_dir, where='fake')
print('=> nDCG (fake): {:.4f}({:.4f})'.format(nDCGs.mean(), nDCGs.std()))
print('=> worst possible nDCG (fake): {:.4f}({:.4f})'.format(worst_nDCGs.mean(), worst_nDCGs.std()))

nDCGs, worst_nDCGs = compute_nDCG_by_bleu(real_img_feats.numpy(), real_txt_feats.numpy(), recipes, retrieved_range=retrieved_range, save_dir=save_dir, where='real')
print('=> nDCG_BLEU (real): {:.4f}({:.4f})'.format(nDCGs.mean(), nDCGs.std()))
print('=> worst possible nDCG_BLEU (real): {:.4f}({:.4f})'.format(worst_nDCGs.mean(), worst_nDCGs.std()))
nDCGs, worst_nDCGs = compute_nDCG_by_bleu(fake_img_feats.numpy(), real_txt_feats.numpy(), recipes, retrieved_range=retrieved_range, save_dir=save_dir, where='fake')
print('=> nDCG_BLEU (fake): {:.4f}({:.4f})'.format(nDCGs.mean(), nDCGs.std()))
print('=> worst possible nDCG_BLEU (fake): {:.4f}({:.4f})'.format(worst_nDCGs.mean(), worst_nDCGs.std()))