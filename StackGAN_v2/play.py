'''
CUDA_VISIBLE_DEVICES=0 python interpolate.py \
    --resume=models/salad_e171.ckpt \
    --food_type=salad --key_ingr=tomato
'''

import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision.utils import save_image
from tqdm import tqdm
import numpy as np
import random
import os
import sys
import math
import pdb
from copy import deepcopy
from glob import glob
from PIL import Image
import json
from gensim.models.keyedvectors import KeyedVectors

from args_StackGAN import args
from datasets_StackGAN import FoodDataset
from networks_StackGAN import G_NET
from utils_StackGAN import load_retrieval_model, prepare_data, compute_img_feat, compute_txt_feat, save_img_results
import sys
sys.path.append('../retrieval_model')
from networks_retrieval import TextEncoder, ImageEncoder, SharedLayer, norm
from datasets_retrieval import choose_one_image, val_transform, Dataset
sys.path.append('../')
from utils import load_recipes, get_title_wordvec, get_ingredients_wordvec, get_instructions_wordvec

# type_ = 'salad'
# key_ingr = ['tomato', 'cucumber', 'black_olive', 'avocado', 'carrot', 'red_pepper']

# type_ = 'cookie'
# key_ingr = ['walnut', 'chocolate', 'coconut', 'molass', 'orange']

# type_ = 'muffin'
# key_ingr = ['blueberry', 'chocolate', 'oat', 'banana', 'cranberry']

assert args.resume

if not args.seed:
    args.seed = random.randint(1, 10000)
random.seed(args.seed)
torch.manual_seed(args.seed)
np.random.seed(args.seed)

device = torch.device('cuda')

food_type = args.food_type
key_ingr = args.key_ingr

model_name = os.path.basename(args.resume).rsplit('.')[0]
save_dir = 'models/{}/play/'.format(model_name)
if not os.path.exists(save_dir):
    os.makedirs(save_dir)

recipes = load_recipes(args.recipe_file, 'test')
recipes = [x for x in recipes if food_type in x['title'].lower()]
print('#{}:'.format(food_type), len(recipes))

text_encoder, image_encoder, shared_layer = load_retrieval_model(args.retrieval_model_ckpt, device)

mean = [0.485, 0.456, 0.406]
std = [0.229, 0.224, 0.225]

print('key_ingr:', key_ingr)
hot_recipes, hot_recipes_, cold_recipes = [], [], []
for rcp in recipes:
    text = rcp['title']
    text += ' '.join(rcp['ingredients'])
    text += '\n'.join(rcp['instructions'])
    if key_ingr in text.lower():
        hot_recipes.append(rcp)
    else:
        cold_recipes.append(rcp)
        continue
        
    rcp_copy = deepcopy(rcp)
    if key_ingr in rcp_copy['title'].lower():
        rcp_copy['title'] = 'A salad'
    rcp_copy['ingredients'] = [x for x in rcp_copy['ingredients'] if key_ingr not in x.lower()]
    if len(rcp_copy['ingredients'])==0:
        continue
    rcp_copy['instructions'] = [x for x in rcp_copy['instructions'] if key_ingr not in x.lower()]
    if len(rcp_copy['instructions'])==0:
        continue
    hot_recipes_.append(rcp_copy)


print('with={}/{} = {:.2f}'.format(
    len(hot_recipes), len(recipes), 1.0*len(hot_recipes)/len(recipes)))


def get_img_feat_from_recipe(rcps, img_dir, transform, image_encoder, shared_layer, device):
    imgs = []
    for rcp in rcps:
        img = choose_one_image(rcp, img_dir)
        imgs.append(transform(img))
    imgs = torch.stack(imgs)
    feats = []
    batch_size = 128
    num_batches = math.ceil(1.0*imgs.shape[0]/batch_size)
    with torch.no_grad():
        for i in tqdm(range(num_batches)):
            feat = norm(shared_layer(image_encoder(imgs[i*batch_size:(i+1)*batch_size].to(device))))
            feats.append(feat)
    feats = torch.cat(feats, dim=0)
    return imgs, feats

wv = KeyedVectors.load(args.word2vec_file, mmap='r')
w2i = {w: i+2 for i, w in enumerate(wv.index2word)}
w2i['<other>'] = 1

def extract_vector(rcp, w2i):
    title, n_words_in_title = get_title_wordvec(rcp, w2i) # np.int [max_len]
    ingredients, n_ingrs = get_ingredients_wordvec(rcp, w2i) # np.int [max_len]
    instructions, n_insts, n_words_each_inst = get_instructions_wordvec(rcp, w2i) # np.int [max_len, max_len]
    return title, n_words_in_title, ingredients, n_ingrs, instructions, n_insts, n_words_each_inst

def get_txt_feat_from_recipe(rcps, w2i, text_encoder, shared_layer, device):
    if 'ingredients' in args.retrieval_model_ckpt:
        txts = [[] for _ in range(2)]
        for rcp in rcps:
            txt = extract_vector(rcp, w2i)
            ingredients, n_ingrs = txt[2], txt[3]
            txts[0].append(ingredients)
            txts[1].append(n_ingrs)
        for i in range(len(txts)):
            txts[i] = torch.tensor(np.stack(txts[i])).to(device)
    elif 'recipe' in args.retrieval_model_ckpt:
        txts = [[] for _ in range(7)]
        for rcp in rcps:
            txt = extract_vector(rcp, w2i)
            for i in range(len(txts)):
                txts[i].append(txt[i])
        for i in range(len(txts)):
            txts[i] = torch.tensor(np.stack(txts[i])).to(device)
    feats = []
    batch_size = 2048
    num_batches = math.ceil(1.0*txts[0].shape[0]/batch_size)
    with torch.no_grad():
        for i in range(num_batches):
            txts_one_batch = [x[batch_size*i: batch_size*(i+1)] for x in txts]
            if 'ingredients' in args.retrieval_model_ckpt:
                feat, _ = text_encoder(*txts_one_batch)
            elif 'recipe' in args.retrieval_model_ckpt:
                feat, _, _, _, _ = text_encoder(*txts_one_batch)
            feat = norm(shared_layer(feat))
            feats.append(feat)
    feats = torch.cat(feats, dim=0)
    return txts, feats


# *****************************************************
print('-' * 40)
print('compute text features for all recipes')
_, txt_feats = get_txt_feat_from_recipe(recipes, w2i, text_encoder, shared_layer, device)
txt_feats = txt_feats.cpu().numpy()


# ******************************************************
print('-' * 40)
print('load pretrained generative model')
netG = G_NET()
netG = nn.DataParallel(netG).to(device).eval()
ckpt = torch.load(args.resume)
netG.load_state_dict(ckpt['netG'])

def compute_ingredient_retrival_score(imgs, txts, tops):
    imgs = imgs / np.linalg.norm(imgs, axis=1)[:, None]
    txts = txts / np.linalg.norm(txts, axis=1)[:, None]
    # retrieve recipe
    sims = np.dot(imgs, txts.T) # [N, N]
    # loop through the N similarities for images
    cvgs = []
    for ii in range(imgs.shape[0]):
        # get a row of similarities for image ii
        sim = sims[ii,:]
        # sort indices in descending order
        sorting = np.argsort(sim)[::-1].tolist()
        topk_idxs = sorting[:tops]
        success = 0.0
        for rcp_idx in topk_idxs:
            rcp = recipes[rcp_idx]
            if any([key_ingr in ingr.lower() for ingr in rcp['ingredients']]):
                success += 1
        cvgs.append(success / tops)
    return np.array(cvgs)


print('-' * 40)
print('compute text features for recipes')
_, hot_txt_feats = get_txt_feat_from_recipe(hot_recipes, w2i, text_encoder, shared_layer, device)
hot_txt_feats = hot_txt_feats

_, cold_txt_feats = get_txt_feat_from_recipe(hot_recipes_, w2i, text_encoder, shared_layer, device)
cold_txt_feats = cold_txt_feats

interpolate_points = [1.0, 0.75, 0.5, 0.25, 0.0]
print('interpolate points:', interpolate_points)
max_num_samples = 64
bs = 16
start = 0
one_noise = torch.zeros(1, 100).to(device)
print('interpolate between hot_recipes and hot_recipes_')
while start*bs < min(len(hot_recipes), max_num_samples):
    print('batch:', start)
    # get real images for current batch
    recipes_batch = hot_recipes[start*bs: (start+1)*bs]
    real_imgs = []
    for rcp in recipes_batch:
        img = choose_one_image(rcp, args.img_dir)
        real_imgs.append(val_transform(img))
    real_imgs = torch.stack(real_imgs)

    # get fake images for current batch
    hot_txt_feats_batch = hot_txt_feats[start*bs: (start+1)*bs]
    cold_txt_feats_batch = cold_txt_feats[start*bs: (start+1)*bs]
    
    fake_imgs_all = []
    for w_y in interpolate_points:
        txt_embedding = w_y*hot_txt_feats_batch + (1-w_y)*cold_txt_feats_batch
        fixed_noise = one_noise.repeat(hot_txt_feats_batch.shape[0], 1)
        with torch.no_grad():
            fake_imgs, _, _ = netG(fixed_noise, txt_embedding)
        imgs = fake_imgs[-1] # [16, 3, 256, 256]
        imgs = imgs/2 + 0.5
        imgs = F.interpolate(imgs, [224, 224], mode='bilinear', align_corners=True)
        for i in range(imgs.shape[1]):
            imgs[:,i] = (imgs[:,i]-mean[i])/std[i]
        fake_imgs_all.append(imgs)

        for i in range(imgs.shape[1]):
            imgs[:,i] = (imgs[:,i]-mean[i])/std[i]
        with torch.no_grad():
            img_feats = image_encoder(imgs).detach().cpu().numpy()
        cvgs = compute_ingredient_retrival_score(img_feats, txt_feats, 5)
        print('with/without={:.2f}/{:.2f}, avg cvg over {} recipes = {:.2f} ({:.2f})'.format(
            w_y, 1-w_y, bs, cvgs.mean(), cvgs.std()))

    fake_imgs_all = torch.stack(fake_imgs_all).cpu() # [5, 16, 3, 224, 224]
    
    # concatenate real and fake images
    imgs_all = torch.cat((real_imgs.unsqueeze(0), fake_imgs_all), dim=0) # [6, 16, 3, 224, 224]
    imgs_all = imgs_all.permute(1,0,2,3,4).contiguous() # [16, 6, 3, 224, 224]
    imgs_all = imgs_all.view(-1, 3, 224, 224) # [96, 3, 224, 224]

    # save images
    save_image(
        imgs_all, 
        os.path.join(save_dir, 'play_{}_{}.jpg'.format(key_ingr, start)), 
        nrow=6, 
        normalize=True, 
        scale_each=True)

    # save recipes
    hot_recipes_batch, cold_recipes_batch = hot_recipes[start*bs: (start+1)*bs], hot_recipes_[start*bs: (start+1)*bs]
    with open(os.path.join(save_dir, 'play_with_{}_{}.json'.format(key_ingr, start)), 'w') as f:
        json.dump(hot_recipes_batch, f, indent=2)
    with open(os.path.join(save_dir, 'play_without_{}_{}.json'.format(key_ingr, start)), 'w') as f:
        json.dump(cold_recipes_batch, f, indent=2)
    
    start += 1


#****************************************************************
print()
print('interpolate between hot_recipes and cold_recipes')
for recipe in recipes:
    ingrediens_list = recipe['ingredients']
    recipe['new_ingrs'] = []
    for name in ingrediens_list:
        if key_ingr in name:
            recipe['new_ingrs'].append(key_ingr)
        else:
            recipe['new_ingrs'].append(name)

recipes_a = []
recipes_b = []
threshold = 0.7
for rcp_a in hot_recipes:
    tmp = deepcopy(rcp_a['new_ingrs'])
    try:
        tmp.remove(key_ingr)
    except:
        pass
    ingrs_a = set(tmp)
    for rcp_b in cold_recipes:
        ingrs_b = set(rcp_b['new_ingrs'])
        union = ingrs_a.union(ingrs_b)
        common = ingrs_a.intersection(ingrs_b)
        if 1.0*len(common)/len(union) >= threshold:
            recipes_a.append(rcp_a)
            recipes_b.append(rcp_b)
print('#{} pairs (IoU={:.2f}) = {}'.format(key_ingr, threshold, len(recipes_a)))

ids_a = set()
uniques_a = []
for rcp in recipes_a:
    if rcp['id'] not in ids_a:
        uniques_a.append(rcp)
        ids_a.add(rcp['id'])
ids_b = set()
uniques_b = []
for rcp in recipes_b:
    if rcp['id'] not in ids_b:
        uniques_b.append(rcp)
        ids_b.add(rcp['id'])
print('#unique = {}, #unique_ = {}'.format(len(uniques_a), len(uniques_b)))

start = 0
while start*bs < min(len(uniques_a), len(uniques_b), max_num_samples):
    print('batch:', start)
    # get real images for current batch
    hot_recipes_batch = uniques_a[start*bs: (start+1)*bs]
    cold_recipes_batch = uniques_b[start*bs: (start+1)*bs]
    hot_real_imgs, cold_real_imgs = [], []
    for hot_rcp, cold_rcp in zip(hot_recipes_batch, cold_recipes_batch):
        hot_img = choose_one_image(hot_rcp,args.img_dir)
        hot_real_imgs.append(val_transform(hot_img))
        cold_img = choose_one_image(cold_rcp, args.img_dir)
        cold_real_imgs.append(val_transform(cold_img))
    hot_real_imgs = torch.stack(hot_real_imgs)
    cold_real_imgs = torch.stack(cold_real_imgs)

    # get fake images for current batch
    _, hot_txt_feats_batch = get_txt_feat_from_recipe(hot_recipes_batch, w2i, text_encoder, shared_layer, device)
    _, cold_txt_feats_batch = get_txt_feat_from_recipe(cold_recipes_batch, w2i, text_encoder, shared_layer, device)
    fake_imgs_all = []
    for w_y in interpolate_points:
        txt_embedding = w_y*hot_txt_feats_batch + (1-w_y)*cold_txt_feats_batch
        fixed_noise = one_noise.repeat(hot_txt_feats_batch.shape[0], 1)
        # pdb.set_trace()
        with torch.no_grad():
            fake_imgs, _, _ = netG(fixed_noise, txt_embedding)
        imgs = fake_imgs[-1] # [16, 3, 256, 256]
        imgs = imgs/2 + 0.5
        imgs = F.interpolate(imgs, [224, 224], mode='bilinear', align_corners=True)
        for i in range(imgs.shape[1]):
            imgs[:,i] = (imgs[:,i]-mean[i])/std[i]
        fake_imgs_all.append(imgs)

        for i in range(imgs.shape[1]):
            imgs[:,i] = (imgs[:,i]-mean[i])/std[i]
        with torch.no_grad():
            img_feats = image_encoder(imgs).detach().cpu().numpy()
        cvgs = compute_ingredient_retrival_score(img_feats, txt_feats, 5)
        print('with/without={:.2f}/{:.2f}, avg cvg over {} recipes = {:.2f} ({:.2f})'.format(
            w_y, 1-w_y, bs, cvgs.mean(), cvgs.std()))
        
    fake_imgs_all = torch.stack(fake_imgs_all).cpu() # [5, 16, 3, 224, 224]
    
    # concatenate real and fake images
    imgs_all = torch.cat((hot_real_imgs.unsqueeze(0), fake_imgs_all, cold_real_imgs.unsqueeze(0)), dim=0) # [7, 16, 3, 224, 224]
    imgs_all = imgs_all.permute(1,0,2,3,4).contiguous() # [16, 7, 3, 224, 224]
    imgs_all = imgs_all.view(-1, 3, 224, 224) # [112, 3, 224, 224]

    # save images
    save_image(
        imgs_all, 
        os.path.join(save_dir, 'interpolate_{}_{}.jpg'.format(key_ingr, start)), 
        nrow=7, 
        normalize=True, 
        scale_each=True)

    # save recipes
    with open(os.path.join(save_dir, 'interpolate_with_{}_{}.json'.format(key_ingr, start)), 'w') as f:
        json.dump(hot_recipes_batch, f, indent=2)
    with open(os.path.join(save_dir, 'interpolate_without_{}_{}.json'.format(key_ingr, start)), 'w') as f:
        json.dump(cold_recipes_batch, f, indent=2)
    
    start += 1