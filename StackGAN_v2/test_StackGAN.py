import torch
from torch import nn
from torchvision import transforms
import torchvision.utils as vutils
import numpy as np
import random
import os
from tqdm import tqdm

from args_StackGAN import args
from datasets_StackGAN import FoodDataset
from networks_StackGAN import G_NET
from utils_StackGAN import load_retrieval_model, prepare_data, compute_img_feat, compute_txt_feat, save_img_results

from metrics.frechet_inception_distance import FIDModel, compute_fid
from metrics.inception_score import INCEPTION_V3, compute_is
from metrics.msssim import compute_msssim
from metrics.ndcg import compute_nDCG_by_sim, compute_nDCG_by_bleu

import sys
sys.path.append('../retrieval_model')
from networks_retrieval import TextEncoder, ImageEncoder, SharedLayer, norm
from utils_retrieval import rank

if not args.resume:
    args.resume = 'models/salad_e171.ckpt'

if not args.seed:
    args.seed = random.randint(1, 10000)
random.seed(args.seed)
torch.manual_seed(args.seed)
np.random.seed(args.seed)
print(args.seed)

device = torch.device('cuda')

imsize = args.base_size * (2 ** (args.levels-1))
image_transform = transforms.Compose([
    transforms.Resize(int(imsize * 76 / 64)),
    transforms.CenterCrop(imsize)
])

dataset = FoodDataset(
    recipe_file=args.recipe_file,
    img_dir=args.img_dir,
    levels=args.levels,
    part='test', 
    food_type=args.food_type,
    base_size=args.base_size, 
    transform=image_transform)

recipes = dataset.recipes

if args.debug:
    print('in debug mode')
    dataset = torch.utils.data.Subset(dataset, range(200))

dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=64,
        drop_last=True, shuffle=False, num_workers=int(args.workers))
print('test data info:', len(dataset), len(dataloader))

recipes = recipes[:64*len(dataloader)]

##############################
# model
##############################
text_encoder, image_encoder, shared_layer = load_retrieval_model(args.retrieval_model_ckpt, device)

netG = G_NET()
inception_model = INCEPTION_V3()
fid_model = FIDModel()

netG, inception_model, fid_model = [nn.DataParallel(x).to(device).eval() for x in [netG, inception_model, fid_model]]
ckpt = torch.load(args.resume)
netG.load_state_dict(ckpt['netG'])
epoch = ckpt['epoch']

##############################
# test
##############################
model_name = os.path.basename(args.resume).rsplit('.')[0]
res = 64 * 2**(args.level)
save_dir = 'models/{}/{}/'.format(model_name, res)
if not os.path.exists(save_dir):
    os.makedirs(save_dir)
fixed_noise = torch.FloatTensor(1, args.z_dim).normal_(0, 1).to(device)
fixed_noise = fixed_noise.repeat(64, 1)

real_txt_feats = []
real_img_feats = []
fake_img_feats = []
real_is_outputs = []
fake_is_outputs = []
real_fid_outputs = []
fake_fid_outputs = []

text_encoder.eval()
image_encoder.eval()
shared_layer.eval()
netG.eval()

batch=0
real_imgs_list = []
fake_imgs_list = []
finish_ssim = False
for data in tqdm(dataloader):
    real_imgs, _, txt = prepare_data(data, device)
    with torch.no_grad():
        txt_feat = compute_txt_feat(txt, text_encoder, shared_layer)
        fake_imgs, mu, logvar = netG(fixed_noise, txt_feat)
        
        real_imgs_list.extend(real_imgs[-1].tolist())
        fake_imgs_list.extend(fake_imgs[-1].tolist())
        if len(fake_imgs_list) > 1000 and not finish_ssim: 
            print('computing ssim...')
            ids_sub = np.random.choice(len(fake_imgs_list), 200, replace=False)

            fake_imgs_list = torch.tensor(fake_imgs_list)[ids_sub]
            msssim = compute_msssim(fake_imgs_list[:100], fake_imgs_list[100:])
            print('msssim (fake)= {:.4f}'.format(msssim))
            fake_imgs_list = []

            real_imgs_list = torch.tensor(real_imgs_list)[ids_sub]
            msssim = compute_msssim(real_imgs_list[:100], real_imgs_list[100:])
            print('msssim (real) = {:.4f}'.format(msssim))
            real_imgs_list = []
            
            finish_ssim = True
    
        real_txt_feats.append(txt_feat.detach().cpu())

        real_img = real_imgs[-1]
        real_img_feat = compute_img_feat(real_img, image_encoder, shared_layer)
        real_img_feats.append(real_img_feat.detach().cpu())

        fake_img = fake_imgs[-1]
        fake_img_feat = compute_img_feat(fake_img, image_encoder, shared_layer)
        fake_img_feats.append(fake_img_feat.detach().cpu())

        real_fake = save_img_results(real_imgs, fake_imgs, save_dir, batch, level=args.level)

        real_is_output = inception_model(real_img.detach())
        real_is_outputs.append(real_is_output.cpu())
        fake_is_output = inception_model(fake_img.detach())
        fake_is_outputs.append(fake_is_output.cpu())

        real_fid_output = fid_model(real_img.detach())
        real_fid_outputs.append(real_fid_output.cpu())
        fake_fid_output = fid_model(fake_img.detach())
        fake_fid_outputs.append(fake_fid_output.cpu())

    if batch == 0:
        noise = torch.FloatTensor(64, args.z_dim).normal_(0, 1).to(device)
        one_txt_feat = txt_feat[0:1]
        one_txt_feat = one_txt_feat.repeat(64, 1)
        fakes, _, _ = netG(noise, one_txt_feat)
        vutils.save_image(
            fakes[-1], 
            os.path.join(save_dir, 'random_noise_image0.jpg'), 
            normalize=True, scale_each=True)
    batch += 1

real_txt_feats = torch.cat(real_txt_feats, dim=0)
real_img_feats = torch.cat(real_img_feats, dim=0)
fake_img_feats = torch.cat(fake_img_feats, dim=0)
retrieved_range = min(900, len(dataloader)*64)

medR, medR_std, recalls = rank(real_txt_feats.numpy(), real_img_feats.numpy(), retrieved_type='recipe', retrieved_range=retrieved_range)
print('=> MedR (real): {:.4f}({:.4f})'.format(medR, medR_std))
medR, medR_std, recalls = rank(real_txt_feats.numpy(), fake_img_feats.numpy(), retrieved_type='recipe', retrieved_range=retrieved_range)
print('=> MedR (fake): {:.4f}({:.4f})'.format(medR, medR_std))

nDCGs, worst_nDCGs = compute_nDCG_by_sim(real_img_feats.numpy(), real_txt_feats.numpy(), retrieved_range=retrieved_range)
print('=> nDCG (real): {:.4f}({:.4f})'.format(nDCGs.mean(), nDCGs.std()))
print('=> worst possible nDCG (real): {:.4f}({:.4f})'.format(worst_nDCGs.mean(), worst_nDCGs.std()))
nDCGs, worst_nDCGs = compute_nDCG_by_sim(fake_img_feats.numpy(), real_txt_feats.numpy(), retrieved_range=retrieved_range)
print('=> nDCG (fake): {:.4f}({:.4f})'.format(nDCGs.mean(), nDCGs.std()))
print('=> worst possible nDCG (fake): {:.4f}({:.4f})'.format(worst_nDCGs.mean(), worst_nDCGs.std()))

# nDCGs, worst_nDCGs = compute_nDCG_by_bleu(real_img_feats.numpy(), real_txt_feats.numpy(), recipes, retrieved_range=retrieved_range)
# print('=> nDCG_BLEU (real): {:.4f}({:.4f})'.format(nDCGs.mean(), nDCGs.std()))
# print('=> worst possible nDCG_BLEU (real): {:.4f}({:.4f})'.format(worst_nDCGs.mean(), worst_nDCGs.std()))
# nDCGs, worst_nDCGs = compute_nDCG_by_bleu(fake_img_feats.numpy(), real_txt_feats.numpy(), recipes, retrieved_range=retrieved_range)
# print('=> nDCG_BLEU (fake): {:.4f}({:.4f})'.format(nDCGs.mean(), nDCGs.std()))
# print('=> worst possible nDCG_BLEU (fake): {:.4f}({:.4f})'.format(worst_nDCGs.mean(), worst_nDCGs.std()))

real_is_outputs = torch.cat(real_is_outputs, 0)
is_mean, is_std = compute_is(real_is_outputs, 2)
print('=> IS (real): {:.4f}({:.4f})'.format(is_mean, is_std))
fake_is_outputs = torch.cat(fake_is_outputs, 0)
is_mean, is_std = compute_is(fake_is_outputs, 2)
print('=> IS (fake): {:.4f}({:.4f})'.format(is_mean, is_std))

real_fid_outputs = torch.cat(real_fid_outputs, 0)
fake_fid_outputs = torch.cat(fake_fid_outputs, 0)
fid = compute_fid(real_fid_outputs, fake_fid_outputs)
print('=> FID = {:.4f}'.format(fid))
