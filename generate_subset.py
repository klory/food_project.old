from tqdm import tqdm
import os
from utils import load_recipes
import argparse
import json
import shutil  
from multiprocessing import Pool
import time

parser = argparse.ArgumentParser(description='Generate subset')
parser.add_argument('--recipe_file', default='data/Recipe1M_plus/recipes_withImage.json')
parser.add_argument('--img_dir', default='data/Recipe1M_plus/images')
parser.add_argument('--food_type', default='salad', choices=['salad', 'cookie'])
args = parser.parse_args()

dst_folder = '/gpu2/fh199/datasets/{}_plus'.format(args.food_type)
os.makedirs(dst_folder, exist_ok=True)

print('load all recipes')
all_recipes = load_recipes(args.recipe_file)
recipes = [x for x in all_recipes if args.food_type in x['title'].lower()]

print('save target recipes')
with open(os.path.join(dst_folder, '{}.json'.format(args.food_type)), 'w') as f:
    json.dump(recipes, f, indent=2)


def process_one_recipe(rcp):
    images = rcp['images']
    for image in images:
        loader_path = [image['id'][i] for i in range(4)]
        loader_path = os.path.join(*loader_path)
        src_path = os.path.join(args.img_dir, loader_path, image['id'])
        dst_path = os.path.join(dst_folder, loader_path, image['id'])
        os.makedirs(os.path.dirname(dst_path), exist_ok=True)
        shutil.copyfile(src_path, dst_path)

# start = time.time()
# print('copy images')
# for rcp in tqdm(recipes):
#     process_one_recipe(rcp)
# print('time = {:.4f}'.format(time.time()-start))

pool = Pool()
start = time.time()
pool.map(process_one_recipe, recipes)
print('time = {:.4f}'.format(time.time()-start))