'''
CUDA_VISIBLE_DEVICES=0 python predict_key_ingredients.py \
    --batch_size=32 --resume=models/recipe_hardMining_sharedLayer.ckpt \
    --food_type=salad --key_ingr=tomato
'''

import torch
from torch import nn
from torchvision.utils import save_image
from tqdm import tqdm
import numpy as np
import random
import os
import sys
import math
import pdb
from copy import deepcopy
from gensim.models.keyedvectors import KeyedVectors

from args_retrieval import args
from networks_retrieval import TextEncoder, IngredientsEncoder, ImageEncoder, SharedLayer, norm
from datasets_retrieval import choose_one_image, val_transform
sys.path.append('../')
from utils import load_recipes, get_title_wordvec, get_ingredients_wordvec, get_instructions_wordvec

# type_ = 'salad'
# key_ingr = ['tomato', 'cucumber', 'black_olife', 'avocado', 'carrot', 'red_pepper']

# type_ = 'cookie'
# key_ingr = ['walnut', 'chocolate', 'coconut', 'molass', 'orange']

# type_ = 'muffin'
# key_ingr = ['blueberry', 'chocolate', 'oat', 'banana', 'cranberry']

assert args.resume

if not args.seed:
    args.seed = random.randint(1, 10000)
random.seed(args.seed)
torch.manual_seed(args.seed)
np.random.seed(args.seed)

device = torch.device('cuda')

food_type = args.food_type
key_ingr = args.key_ingr
tops = 5

model_name = os.path.basename(args.resume).rsplit('.')[0]
save_dir = 'models/{}/'.format(model_name)
if not os.path.exists(save_dir):
    os.makedirs(save_dir)

recipes = load_recipes(args.recipe_file, 'test')
recipes = [x for x in recipes if food_type in x['title'].lower()]
print('# {} recipes:'.format(food_type), len(recipes))

for recipe in recipes:
    ingrediens_list = recipe['ingredients']
    recipe['new_ingrs'] = []
    for name in ingrediens_list:
        if key_ingr in name:
            recipe['new_ingrs'].append(key_ingr)
        else:
            recipe['new_ingrs'].append(name)

if 'ingredients' in args.resume:
    text_encoder = IngredientsEncoder(
        emb_dim=300, 
        hid_dim=300, 
        z_dim=1024, 
        word2vec_file='models/word2vec_recipes.bin', 
        with_attention=2, 
        with_word2vec=True)
elif 'recipe' in args.resume:
    text_encoder = TextEncoder(
        emb_dim=300, 
        hid_dim=300, 
        z_dim=1024, 
        word2vec_file='models/word2vec_recipes.bin', 
        with_attention=2, 
        with_word2vec=True)
image_encoder = ImageEncoder(z_dim=1024)
shared_layer = SharedLayer(z_dim=1024)
text_encoder, image_encoder, shared_layer = [nn.DataParallel(x).eval().to(device) for x in [text_encoder, image_encoder, shared_layer]]
print('load from:', args.resume)
ckpt = torch.load(args.resume)
text_encoder.load_state_dict(ckpt['text_encoder'])
image_encoder.load_state_dict(ckpt['image_encoder'])
shared_layer.load_state_dict(ckpt['shared_layer'])

print('key_ingr:', key_ingr)
hot_recipes = [x for x in recipes if key_ingr in x['new_ingrs']]
cold_recipes = [x for x in recipes if key_ingr not in x['new_ingrs']]
print('with={}/{} = {:.2f}, without={}/{} = {:.2f}'.format(
    len(hot_recipes), len(recipes), 1.0*len(hot_recipes)/len(recipes), 
    len(cold_recipes), len(recipes), 1.0*len(cold_recipes)/len(recipes)))

recipes_a = []
recipes_b = []
threshold = 0.7
for rcp_a in hot_recipes:
    tmp = deepcopy(rcp_a['new_ingrs'])
    tmp.remove(key_ingr)
    ingrs_a = set(tmp)
    for rcp_b in cold_recipes:
        ingrs_b = set(rcp_b['new_ingrs'])
        union = ingrs_a.union(ingrs_b)
        common = ingrs_a.intersection(ingrs_b)
        if 1.0 * len(common) / len(union) >= threshold:
            recipes_a.append(rcp_a)
            recipes_b.append(rcp_b)
print('#{} pairs (IoU={:.2f}) = {}'.format(key_ingr, threshold, len(recipes_a)))

ids_a = set()
uniques_a = []
for rcp in recipes_a:
    if rcp['id'] not in ids_a:
        uniques_a.append(rcp)
        ids_a.add(rcp['id'])
ids_b = set()
uniques_b = []
for rcp in recipes_b:
    if rcp['id'] not in ids_b:
        uniques_b.append(rcp)
        ids_b.add(rcp['id'])
print('#unique = {}, #unique_ = {}'.format(len(uniques_a), len(uniques_b)))

#####################################
print('-' * 40)
#####################################

def get_img_feat_from_recipe(rcps, img_dir, transform, image_encoder, shared_layer, device):
    imgs = []
    for rcp in rcps:
        img = choose_one_image(rcp, img_dir)
        imgs.append(transform(img))
    imgs = torch.stack(imgs)
    feats = []
    batch_size = 128
    num_batches = math.ceil(1.0*imgs.shape[0]/batch_size)
    with torch.no_grad():
        for i in tqdm(range(num_batches)):
            feat = norm(shared_layer(image_encoder(imgs[i*batch_size:(i+1)*batch_size].to(device))))
            feats.append(feat)
    feats = torch.cat(feats, dim=0)
    return imgs, feats

wv = KeyedVectors.load(args.word2vec_file, mmap='r')
w2i = {w: i+2 for i, w in enumerate(wv.index2word)}
w2i['<other>'] = 1

def extract_vector(rcp, w2i):
    title, n_words_in_title = get_title_wordvec(rcp, w2i) # np.int [max_len]
    ingredients, n_ingrs = get_ingredients_wordvec(rcp, w2i) # np.int [max_len]
    instructions, n_insts, n_words_each_inst = get_instructions_wordvec(rcp, w2i) # np.int [max_len, max_len]
    return title, n_words_in_title, ingredients, n_ingrs, instructions, n_insts, n_words_each_inst

def get_txt_feat_from_recipe(rcps, w2i, text_encoder, shared_layer, device):
    if 'ingredients' in args.resume:
        txts = [[] for _ in range(2)]
        for rcp in rcps:
            txt = extract_vector(rcp, w2i)
            ingredients, n_ingrs = txt[2], txt[3]
            txts[0].append(ingredients)
            txts[1].append(n_ingrs)
        for i in range(len(txts)):
            txts[i] = torch.tensor(np.stack(txts[i])).to(device)
    elif 'recipe' in args.resume:
        txts = [[] for _ in range(7)]
        for rcp in rcps:
            txt = extract_vector(rcp, w2i)
            for i in range(len(txts)):
                txts[i].append(txt[i])
        for i in range(len(txts)):
            txts[i] = torch.tensor(np.stack(txts[i])).to(device)
    feats = []
    batch_size = 2048
    num_batches = math.ceil(1.0*txts[0].shape[0]/batch_size)
    with torch.no_grad():
        for i in tqdm(range(num_batches)):
            txts_one_batch = [x[batch_size*i: batch_size*(i+1)] for x in txts]
            if 'ingredients' in args.resume:
                feat, _ = text_encoder(*txts_one_batch)
            elif 'recipe' in args.resume:
                feat, _, _, _, _ = text_encoder(*txts_one_batch)
            feat = norm(shared_layer(feat))
            feats.append(feat)
    feats = torch.cat(feats, dim=0)
    return txts, feats

# pdb.set_trace()
if len(uniques_a)>1 and len(uniques_b)>1:
    print('compute REAL image features for recipes WITH {}'.format(key_ingr))
    img_a, img_feat_a = get_img_feat_from_recipe(uniques_a, args.img_dir, val_transform, image_encoder, shared_layer, device)
    save_image(
        img_a[:64], 
        os.path.join(save_dir, '{}_with.jpg'.format(key_ingr)), 
        normalize=True)
    img_feat_a = img_feat_a.detach().cpu().numpy()
    
    print('compute REAL image features for recipes WITHOUT {}'.format(key_ingr))
    img_b, img_feat_b = get_img_feat_from_recipe(uniques_b, args.img_dir, val_transform, image_encoder, shared_layer, device)
    save_image(
        img_b[:64], 
        os.path.join(save_dir, '{}_without.jpg'.format(key_ingr)),
        normalize=True)
    img_feat_b = img_feat_b.detach().cpu().numpy()
else:
    print('unable to compute')
    sys.exit(-1)


# ******************************************************
print('-' * 40)
print('compute text features for all recipes')
# *****************************************************
_, txt_feats = get_txt_feat_from_recipe(recipes, w2i, text_encoder, shared_layer, device)
txt_feats = txt_feats.cpu().numpy()

# ******************************************************
print('-' * 40)
print('compute coverage among the top {} retrieved recipes'.format(tops))
# *****************************************************
def compute_ingredient_retrival_score(imgs, txts, tops):
    imgs = imgs / np.linalg.norm(imgs, axis=1)[:, None]
    txts = txts / np.linalg.norm(txts, axis=1)[:, None]
    # retrieve recipe
    sims = np.dot(imgs, txts.T) # [N, N]
    # loop through the N similarities for images
    cvgs = []
    for ii in range(imgs.shape[0]):
        # get a row of similarities for image ii
        sim = sims[ii,:]
        # sort indices in descending order
        sorting = np.argsort(sim)[::-1].tolist()
        topk_idxs = sorting[:tops]
        success = 0.0
        for rcp_idx in topk_idxs:
            rcp = recipes[rcp_idx]
            ingrs = rcp['new_ingrs']
            if key_ingr in ingrs:
                success += 1
        cvgs.append(success / tops)
    return np.array(cvgs)

cvgs = compute_ingredient_retrival_score(img_feat_a, txt_feats, tops)
print('avg coverage on top {} with {} (#={}) = {:.2f} ({:.2f})'.format(
    tops, key_ingr, len(uniques_a), cvgs.mean(), cvgs.std()))
cvgs = compute_ingredient_retrival_score(img_feat_b, txt_feats, tops)
print('avg coverage on top {} without {} (#={}) = {:.2f} ({:.2f})'.format(
    tops, key_ingr, len(uniques_b), cvgs.mean(), cvgs.std()))