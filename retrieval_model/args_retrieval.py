import argparse
import sys
sys.path.append('../')
from utils import str2bool

parser = argparse.ArgumentParser(description='retrieval model parameters')
parser.add_argument('--seed', default=8, type=int)
parser.add_argument('--workers', default=16, type=int)
parser.add_argument("--cuda", type=str2bool, default=True, help="use cuda or not")
parser.add_argument('--word2vec_dim', default=300, type=int)
parser.add_argument('--rnn_hid_dim', default=300, type=int)
parser.add_argument('--feature_dim', default=1024, type=int)
parser.add_argument('--batch_size', default=64, type=int)
parser.add_argument('--epochs', default=50, type=int)
parser.add_argument('--lr', default=1e-4, type=float)
parser.add_argument('--margin', default=0.3, type=float)
parser.add_argument('--recipe_file', default='../data/Recipe1M/recipes_withImage.json')
parser.add_argument('--word2vec_file', default='models/word2vec_recipes.bin')
parser.add_argument('--vocab_ingrs_file', default='../manual_files/list_of_merged_ingredients.txt')
parser.add_argument('--classes_file', default='../data/Recipe1M/classes1M.pkl')
parser.add_argument('--img_dir', default='../data/Recipe1M/images')

parser.add_argument('--retrieved_type', default='recipe', choices=['recipe', 'image'])
parser.add_argument('--retrieved_range', default=1000, type=int)
parser.add_argument('--val_freq', default=1, type=int)
parser.add_argument('--save_freq', default=1, type=int)
parser.add_argument('--resume', default='')

# word2vec
parser.add_argument("--with_word2vec", type=str2bool, default=True, help="use pretrained word2vec")
# upmc
parser.add_argument('--upmc_model', default='pretrain_upmc/models/upmc_resnet50.ckpt')
# permute ingredients
parser.add_argument("--permute_ingrs", type=str2bool, default=False, help="permute ingredient order before input")
# self attention on text
parser.add_argument("--with_attention", type=int, default=2, choices=[0,1,2])

# in debug mode
parser.add_argument("--debug", type=str2bool, default=False, help="in debug mode or not")

# for test_retrieval.py
parser.add_argument("--preload", type=str2bool, default=False, help="load extracted features")

# These are only for predict_key_ingredients.py
parser.add_argument('--food_type', type=str, default='salad')
parser.add_argument('--key_ingr', type=str, default='tomato')

args = parser.parse_args()