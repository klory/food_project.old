import os
import json
import numpy as np
from torchvision import transforms
import json
from matplotlib import pyplot as plt

def rank(rcps, imgs, retrieved_type='recipe', retrieved_range=1000, draw_hist=False):
    N = retrieved_range
    data_size = imgs.shape[0]
    idxs = range(N)
    glob_rank = []
    glob_recall = {1:0.0, 5:0.0, 10:0.0}
    if draw_hist:
        plt.figure(figsize=(16, 6))
    # average over 10 sets
    for i in range(10):
        ids_sub = np.random.choice(data_size, N, replace=False)
        imgs_sub = imgs[ids_sub, :]
        rcps_sub = rcps[ids_sub, :]
        imgs_sub = imgs_sub / np.linalg.norm(imgs_sub, axis=1)[:, None]
        rcps_sub = rcps_sub / np.linalg.norm(rcps_sub, axis=1)[:, None]
        if retrieved_type == 'recipe':
            sims = np.dot(imgs_sub, rcps_sub.T) # [N, N]
        else:
            sims = np.dot(rcps_sub, imgs_sub.T)
        ranks = []
        recall = {1:0.0, 5:0.0, 10:0.0}
        # loop through the N similarities for images
        for ii in idxs:
            # get a column of similarities for image ii
            sim = sims[ii,:]
            # sort indices in descending order
            sorting = np.argsort(sim)[::-1].tolist()
            # find where the index of the pair sample ended up in the sorting
            pos = sorting.index(ii)
            if (pos+1) == 1:
                recall[1]+=1
            if (pos+1) <=5:
                recall[5]+=1
            if (pos+1)<=10:
                recall[10]+=1
            # store the position
            ranks.append(pos+1)

        for ii in recall.keys():
            recall[ii] = recall[ii]/N
        med = int(np.median(ranks))
        for ii in recall.keys():
            glob_recall[ii] += recall[ii]
        glob_rank.append(med)
        if draw_hist:
            ranks = np.array(ranks)
            plt.subplot(2,5,i+1)
            n, bins, patches = plt.hist(x=ranks, bins='auto', color='#0504aa', alpha=0.7, rwidth=0.85)
            plt.grid(axis='y', alpha=0.75)
            plt.ylim(top=300)
            # plt.xlabel('Rank')
            # plt.ylabel('Frequency')
            # plt.title('Rank Distribution')
            plt.text(23, 45, 'avgR(std) = {:.2f}({:.2f})\nmedR={:.2f}\n#<{:d}:{:d}|#={:d}:{:d}|#>{:d}:{:d}'.format(
                np.mean(ranks), np.std(ranks), np.median(ranks), 
                med,(ranks<med).sum(), med,(ranks==med).sum(), med,(ranks>med).sum()))
    if draw_hist:
        plt.savefig('hist.png')
    
    for i in glob_recall.keys():
        glob_recall[i] = glob_recall[i]/10
    return np.mean(glob_rank), np.std(glob_rank), glob_recall