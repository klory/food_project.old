# Python 3.6, PyTorch 0.4
import torch
from torch.utils.data import Subset
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torchvision import datasets, models, transforms
import os
import argparse
import copy
from tqdm import tqdm
from tensorboardX import SummaryWriter
from dataset_upmc import Dataset
import sys
sys.path.append('../')
from utils import param_counter, make_saveDir
from utils_upmc import gen_filelist
import pdb

# 
# arguments
parser = argparse.ArgumentParser(
    description='Resnet50 UMPC-Food-101 Classifier')
parser.add_argument('--batch_size', type=int, default=64)
parser.add_argument('--epochs', type=int, default=25)
parser.add_argument('--data_dir', default='UPMC-Food-101/')
args = parser.parse_args()
print(args)

batch_size = args.batch_size
epochs = args.epochs
data_dir = args.data_dir

#
# load data
parts = ('train', 'test')
for part in parts:
    gen_filelist(data_dir, part)

normalize = transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
data_transforms = {
    parts[0]: transforms.Compose([
        transforms.RandomResizedCrop(224),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        normalize
    ]),
    parts[1]: transforms.Compose([
        transforms.Resize((224,224)),
        transforms.ToTensor(),
        normalize
    ]),
}

datasets = {
    x: Dataset(
        root=os.path.join(data_dir, 'images'), 
        flist=os.path.join(data_dir, x+".txt"), 
        transform=data_transforms[x]) 
    for x in parts}

# datasets = {x: Subset(datasets[x], range(200)) for x in parts}

dataloaders = {
    x: torch.utils.data.DataLoader(
        datasets[x], 
        batch_size=batch_size,
        shuffle=True, num_workers=4, pin_memory=False)
    for x in parts}

dataset_sizes = {x: len(datasets[x]) for x in parts}
dataloader_sizes = {x: len(dataloaders[x]) for x in parts}
print('datasets', dataset_sizes)
print('dataloaders', dataloader_sizes)

#
#  load model
model = models.resnet50(pretrained=True)
num_feat = model.fc.in_features
model.fc = nn.Linear(num_feat, 101)
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters())
scheduler = lr_scheduler.ReduceLROnPlateau(optimizer, patience=5)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = nn.DataParallel(model.to(device))
print('# parameters:', param_counter(model.parameters()))

save_dir = make_saveDir('runs/samples{}'.format(dataset_sizes['train']), args)
writer = SummaryWriter(save_dir)

#
# train
weights_best = copy.deepcopy(model.state_dict())
acc_best = 0.0
n_iter = 0
for epoch in range(epochs):
    print('-' * 10)
    print('Epoch {}/{}'.format(epoch, epochs - 1))
    # Each epoch has a training and validation part
    for part in parts:
        if part == 'train':
            model.train()  # Set model to training mode
        else:
            model.eval()   # Set model to evaluate mode

        running_loss = 0.0
        running_corrects = 0
        # Iterate over data.
        for inputs, labels in tqdm(dataloaders[part]):
            inputs = inputs.to(device)
            labels = labels.to(device)
            optimizer.zero_grad()
            with torch.set_grad_enabled(part == 'train'):
                outputs = model(inputs)
                _, preds = torch.max(outputs, 1)
                loss = criterion(outputs, labels)
                if part == 'train':
                    writer.add_scalar('loss_batch', loss, n_iter)
                    n_iter += 1
                    loss.backward()
                    optimizer.step()

            running_loss += loss.item() * inputs.size(0)
            running_corrects += torch.sum(preds == labels)

        loss_epoch = running_loss / dataset_sizes[part]
        acc_epoch = running_corrects.double() / dataset_sizes[part]
        writer.add_scalar('epoch_loss_{}'.format(part), loss_epoch, epoch)
        writer.add_scalar('epoch_acc_{}'.format(part), acc_epoch, epoch)
        if part == 'train':
            writer.add_scalar('lr', optimizer.param_groups[0]['lr'], epoch)
            if (epoch+1) % 5 == 0:
                print('save checkpoint...')
                torch.save(model.state_dict(), '{}/e{}.ckpt'.format(save_dir, epoch))
        else:
            scheduler.step(loss_epoch)
        print('\n{} Loss: {:.4f} Acc: {:.4f}'.format(
            part, loss_epoch, acc_epoch))
        # deep copy the model
        if part == parts[1] and acc_epoch > acc_best:
            acc_best = acc_epoch
            weights_best = copy.deepcopy(model.state_dict())
            
# load best model weights
print('Best val acc: {:4f}'.format(acc_best))
model.load_state_dict(weights_best)
print('Saving model...')
torch.save(model.state_dict(), '{}/best.ckpt'.format(save_dir))