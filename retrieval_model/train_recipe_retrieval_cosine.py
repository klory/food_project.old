import torch
from torch import nn, optim, autograd
from torch.utils.data import DataLoader
import torchvision
import torchvision.utils as vutils
from torch.optim import lr_scheduler
from torchvision import transforms
from tqdm import tqdm
import numpy as np
import os
from torch.utils.tensorboard import SummaryWriter
import pdb
import pickle
import pprint

from args_retrieval import args
from utils_retrieval import rank 
from datasets_retrieval import Dataset, train_transform, val_transform
from networks_retrieval import TextEncoder, ImageEncoder, SharedLayer, norm
import sys
sys.path.append('../')
from utils import param_counter, make_saveDir

# fix the 
# Exception ignored in: <bound method Image.__del__ of <tkinter.PhotoImage object...
# bug
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt


##############################
# setup
##############################
torch.manual_seed(args.seed)
np.random.seed(args.seed)
device = torch.device('cuda')

pp = pprint.PrettyPrinter(indent=2)
pp.pprint(args.__dict__)

##############################
# dataset
##############################
train_set = Dataset(
    part='train', 
    recipe_file=args.recipe_file,
    img_dir=args.img_dir, 
    word2vec_file=args.word2vec_file, 
    transform=train_transform, 
    permute_ingrs=args.permute_ingrs)

i2w = {i:w for w,i in train_set.w2i.items()}

val_set = Dataset(
    part='val', 
    recipe_file=args.recipe_file,
    img_dir=args.img_dir, 
    word2vec_file=args.word2vec_file, 
    transform=val_transform, 
    permute_ingrs=args.permute_ingrs)

if args.debug:
    print('in debug mode')
    train_set = torch.utils.data.Subset(train_set, range(2000))
    val_set = torch.utils.data.Subset(val_set, range(2000))

train_loader = DataLoader(
    train_set, batch_size=args.batch_size, shuffle=True,
    num_workers=args.workers, pin_memory=True, drop_last=True)
val_loader = DataLoader(
    val_set, batch_size=args.batch_size, shuffle=False,
    num_workers=args.workers, pin_memory=True, drop_last=False)
print('train data:', len(train_set), len(train_loader))
print('val data:', len(val_set), len(val_loader))

##########################
# model
##########################
text_encoder = TextEncoder(
    emb_dim=args.word2vec_dim, 
    hid_dim=args.rnn_hid_dim, 
    z_dim=args.feature_dim, 
    word2vec_file=args.word2vec_file, 
    with_attention=args.with_attention, 
    with_word2vec=args.with_word2vec).to(device)
image_encoder = ImageEncoder(
    z_dim=args.feature_dim, 
    ckpt_path=args.upmc_model).to(device)

optimizer = torch.optim.Adam([
        {'params': text_encoder.parameters(), 'lr':1e-6},
        {'params': image_encoder.parameters(), 'lr':1e-6},
    ], lr=args.lr, betas=(0.5, 0.999))
scheduler = lr_scheduler.ReduceLROnPlateau(optimizer, mode='max', patience=5)

print('# text_encoder', param_counter(text_encoder.parameters()))
print('# image_encoder', param_counter(image_encoder.parameters()))

text_encoder = nn.DataParallel(text_encoder)
image_encoder = nn.DataParallel(image_encoder)

#####################
# train
#####################
def compute_loss(txt, img, device):
    BS = txt.shape[0]
    denom = img.norm(p=2, dim=1, keepdim=True) @ txt.norm(p=2, dim=1, keepdim=True).t()
    numer = img @ txt.t()
    sim = numer / (denom+1e-8)
    margin = 0.3 * torch.ones_like(sim)
    mask = torch.eye(margin.shape[0], margin.shape[1]).byte().to(device)
    margin.masked_fill_(mask, 0)
    cor_sim = (torch.diag(sim) * torch.ones(BS, BS).to(device)).t()
    loss_retrieve_txt = torch.max(
        torch.tensor(0.0).to(device), 
        margin + sim - cor_sim)
    loss_retrieve_img = torch.max(
        torch.tensor(0.0).to(device), 
        margin + sim.t() - cor_sim)
    loss = loss_retrieve_img + loss_retrieve_txt
    # effective number of pairs is BS*BS-BS, those on the diagnal are never counted and always zero
    loss = loss.sum() / (BS*BS-BS) / 2.0
    return loss


title = 'runs/samples{}'.format(len(train_set))
save_dir = make_saveDir(title, args)
writer = SummaryWriter(save_dir)

epoch_start = 0
epoch_end = args.epochs
niter = 0
def _load_model(path):
    global epoch_start, epoch_end, niter
    print('load from ckpt:', path)
    ckpt = torch.load(path)
    text_encoder.load_state_dict(ckpt['text_encoder'])
    image_encoder.load_state_dict(ckpt['image_encoder'])
    optimizer.load_state_dict(ckpt['optimizer'])
    epoch_start = ckpt['epoch'] + 1
    epoch_end = epoch_start + args.epochs
    niter = ckpt['niter_train'] + 1

if args.resume:
    _load_model(args.resume)

def _save_model(epoch):
    global niter
    path = os.path.join(save_dir, 'e{}.ckpt'.format(epoch))
    print('save checkpoint:', path)
    ckpt = {
        'text_encoder': text_encoder.state_dict(),
        'image_encoder': image_encoder.state_dict(),
        'optimizer': optimizer.state_dict(),
        'epoch': epoch,
        'niter_train': niter-1,
    }
    torch.save(ckpt, path)


def _train(epoch):
    global niter
    print('=> train')
    text_encoder.train()
    image_encoder.train()
    loss_epoch = 0.0
    for data in tqdm(train_loader):
        txt, img = data
        for i in range(len(txt)):
            txt[i] = txt[i].to(device)
        img = img.to(device)

        bs = img.shape[0]
        
        if args.with_attention:
            txt_feat, _, _, _, _ = text_encoder(*txt)
        else:
            txt_feat = text_encoder(*txt)

        img_feat = image_encoder(img)    
        loss = compute_loss(txt_feat, img_feat, device)

        writer.add_scalar('loss', loss, niter)
        loss_epoch += loss * bs

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        niter += 1
    loss_epoch /= len(train_set)
    writer.add_scalar('loss_epoch_train', loss_epoch, epoch)

def _val(epoch):
    print('=> val')
    text_encoder.eval()
    image_encoder.eval()
    txt_feats = []
    img_feats = []
    batch = 0
    for data in tqdm(val_loader):
        txt, img = data
        for i in range(len(txt)):
            txt[i] = txt[i].to(device)
        img = img.to(device)

        with torch.no_grad():
            if args.with_attention:
                txt_feat, alpha_title, alpha_ingredients, alpha_instructions, alpha_words_each_inst = text_encoder(*txt)
            else:
                txt_feat = text_encoder(*txt)

            img_feat = image_encoder(img)

        txt_feats.append(txt_feat.detach().cpu())
        img_feats.append(img_feat.detach().cpu())

        # visualize attention
        if args.with_attention and batch==0:
            alpha = alpha_title.unsqueeze(0)
            other_channel = torch.zeros_like(alpha)
            other_channel[alpha==0] = 1.0
            alpha = torch.cat([alpha, other_channel, other_channel])
            writer.add_image('attention_val_title', alpha, epoch)

            alpha = alpha_ingredients.unsqueeze(0)
            other_channel = torch.zeros_like(alpha)
            other_channel[alpha==0] = 1.0
            alpha = torch.cat([alpha, other_channel, other_channel])
            writer.add_image('attention_val_ingredients', alpha, epoch)

            alpha = alpha_instructions.unsqueeze(0)
            other_channel = torch.zeros_like(alpha)
            other_channel[alpha==0] = 1.0
            alpha = torch.cat([alpha, other_channel, other_channel])
            writer.add_image('attention_val_instructions', alpha, epoch)

            title, title_len, ingredients, n_ingrs, instructions, n_insts, insts_lens = txt
            for i in range(title.shape[0]):
                fig = plt.figure(figsize=(10,12))
                # title
                one_vector = title[i].detach().cpu().numpy()
                one_alpha = alpha_title[i].detach().cpu().numpy()
                length = len(one_vector.nonzero()[0])
                one_word_list = [i2w[idx] for idx in one_vector[:length]]
                ind = np.arange(length)
                plt.subplot(411)
                # pdb.set_trace()
                plt.barh(ind, one_alpha[:length])
                plt.yticks(ind, one_word_list)

                # ingredients
                one_vector = ingredients[i].detach().cpu().numpy()
                one_alpha = alpha_ingredients[i].detach().cpu().numpy()
                length = len(one_vector.nonzero()[0])
                one_word_list = [i2w[idx] for idx in one_vector[:length]]
                ind = np.arange(length)
                plt.subplot(412)
                plt.barh(ind, one_alpha[:length])
                plt.yticks(ind, one_word_list)

                # instructions
                one_matrix = instructions[i].detach().cpu().numpy()
                one_alpha = alpha_instructions[i].detach().cpu().numpy()
                # pdb.set_trace()
                length = one_matrix.nonzero()[0].max() + 1
                one_sentence_list = []
                for k in range(length):
                    one_vector = one_matrix[k]
                    one_vector_length = len(one_vector.nonzero()[0])
                    one_sentence = ' '.join([i2w[idx] for idx in one_vector[:one_vector_length]])
                    one_sentence_list.append(one_sentence)
                ind = np.arange(length)
                plt.subplot(413)
                plt.barh(ind, one_alpha[:length])
                plt.yticks(ind, one_sentence_list)

                # images
                plt.subplot(414)
                one_img = img[i].permute(1,2,0).detach().cpu().numpy()
                scale = one_img.max() - one_img.min()
                one_img = (one_img - one_img.min()) / scale
                plt.imshow(one_img)
                plt.axis('off')

                # plt.tight_layout()
                writer.add_figure('attention_val_{}'.format(i), fig, epoch)
        batch += 1

    txt_feats = torch.cat(txt_feats, dim=0)
    img_feats = torch.cat(img_feats, dim=0)

    retrieved_range = min(txt_feats.shape[0], args.retrieved_range)
    print('retrieved_range =', retrieved_range)
    medR, medR_std, recalls = rank(
        txt_feats.numpy(), 
        img_feats.numpy(), 
        retrieved_type=args.retrieved_type, 
        retrieved_range=retrieved_range)
    writer.add_scalar('MedR', medR, epoch)
    writer.add_scalar('MedR_std', medR_std, epoch)
    print('MedR = {:.4f}({:.4f})'.format(medR, medR_std))
    for k,v in recalls.items():
        print('Recall@{} = {:.4f}'.format(k, v))
    for k,v in recalls.items():
        writer.add_scalar('Recall@{}'.format(k), v, epoch)
    scheduler.step(recalls[1])


for epoch in range(epoch_start, epoch_end):
    print('-' * 40)
    print('=> Epoch: {}/{}'.format(epoch, epoch_end-1))
    _train(epoch)
    if (epoch+1) % args.val_freq == 0:
        _val(epoch)
    if (epoch+1) % args.save_freq == 0 or epoch+1 == args.epochs:
        _save_model(epoch)