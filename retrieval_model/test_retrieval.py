import torch
from torch import nn
from torch.utils.data import DataLoader
from tqdm import tqdm
import numpy as np
import os
import pdb
import pprint

from args_retrieval import args
from utils_retrieval import rank 
from datasets_retrieval import Dataset, val_transform
from networks_retrieval import TextEncoder, ImageEncoder, SharedLayer, norm

def extract_features():
    dataset = Dataset(
        part='test', 
        recipe_file=args.recipe_file,
        img_dir=args.img_dir, 
        word2vec_file=args.word2vec_file, 
        transform=val_transform, 
        permute_ingrs=args.permute_ingrs)

    if args.debug:
        print('in debug mode')
        dataset = torch.utils.data.Subset(dataset, range(2000))

    dataloader = DataLoader(
        dataset, batch_size=args.batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True, drop_last=False)
    print('test data:', len(dataset), len(dataloader))

    text_encoder = TextEncoder(
        emb_dim=args.word2vec_dim, 
        hid_dim=args.rnn_hid_dim, 
        z_dim=args.feature_dim, 
        word2vec_file=args.word2vec_file, 
        with_attention=args.with_attention, 
        with_word2vec=args.with_word2vec).to(device)
    image_encoder = ImageEncoder(
        z_dim=args.feature_dim, 
        ckpt_path=args.upmc_model).to(device)

    text_encoder = nn.DataParallel(text_encoder)
    image_encoder = nn.DataParallel(image_encoder)

    if args.resume:
        path = args.resume
        print('load from ckpt:', path)
        ckpt = torch.load(path)
        text_encoder.load_state_dict(ckpt['text_encoder'])
        image_encoder.load_state_dict(ckpt['image_encoder'])


    text_encoder.eval()
    image_encoder.eval()
    txt_feats = []
    img_feats = []
    for data in tqdm(dataloader):
        txt, img = data
        for i in range(len(txt)):
            txt[i] = txt[i].to(device)
        img = img.to(device)

        with torch.no_grad():
            if args.with_attention:
                txt_feat, _, _, _, _ = text_encoder(*txt)
            else:
                txt_feat = text_encoder(*txt)

            img_feat = image_encoder(img)

            txt_feats.append(txt_feat.detach().cpu())
            img_feats.append(img_feat.detach().cpu())

    txt_feats = torch.cat(txt_feats, dim=0)
    img_feats = torch.cat(img_feats, dim=0)
    return txt_feats, img_feats


if __name__ == '__main__':
    torch.manual_seed(args.seed)
    np.random.seed(args.seed)
    device = torch.device('cuda')

    pp = pprint.PrettyPrinter(indent=2)
    pp.pprint(args.__dict__)

    txt_feats_path = 'models/test_txt_feats.pt'
    img_feats_path = 'models/test_img_feats.pt'
    if args.preload and os.path.exists(txt_feats_path):
        txt_feats = torch.load(txt_feats_path)
        img_feats = torch.load(img_feats_path)
    else:
        txt_feats, img_feats = extract_features()
        torch.save(txt_feats, txt_feats_path)
        torch.save(img_feats, img_feats_path)

    retrieved_range = min(txt_feats.shape[0], args.retrieved_range)
    print('retrieved_range =', retrieved_range)
    medR, medR_std, recalls = rank(
        txt_feats.numpy(), 
        img_feats.numpy(), 
        retrieved_type=args.retrieved_type, 
        retrieved_range=retrieved_range)
    print('MedR = {:.4f}({:.4f})'.format(medR, medR_std))
    for k,v in recalls.items():
        print('Recall@{} = {:.4f}'.format(k, v))
